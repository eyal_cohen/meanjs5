'use strict';

angular.module('core').controller('HomeController', ['$scope', '$http', '$compile', 'leafletData', 'Authentication',
  function ($scope, $http, $compile, leafletData, Authentication) {
    var vm = this;

    // This provides Authentication context.
    $scope.authentication = Authentication;


    /************/
    /** SLIDER **/
    /************/
    $scope.sliderPos = [];
    $scope.tripsCount = [];
    $scope.sliderNext = function(id) {
      console.log('next');
      if($scope.sliderPos[id] + 4 < $scope.tripsCount[id]) {
        $scope.sliderPos[id]++;
      }
    };
    $scope.sliderPrev = function(id) {
      console.log('prev');
      if($scope.sliderPos[id] > 0) {
        $scope.sliderPos[id]--;
      }
    };
    /***********/

    var pruneCluster = new window.PruneClusterForLeaflet();
    pruneCluster.Cluster.Size = 10;

    var buildMarkerPopUp = function(trip) {
      var popup = '';
      console.log(trip);
      var div_start = '<div style="width: 220px;">';
      var image = '<a href="trips/' + trip._id + '"><img style="width: 100%;" src="http://s3-us-west-2.amazonaws.com/triptip-files/200_150/' + trip.themeImage + '"></a>';
      var title = '<a href="trips/' + trip._id + '" style="display: block; text-align: center; padding: 5px 0;"><h5>' + trip.title + '</h5></a>';
      var user = '<p>By ' + trip.user.displayName + '</p>';
      var subtitle = '<p>' + trip.subtitle + '</p>';
      var read = '<a href="trips/' + trip._id + '">Read >></a>';
      var div_end = '</div>';
      popup = div_start + image + title + div_end;
      return popup;
    };

    var makeMarkers = function(trips) {
      var marker;
      angular.forEach(trips, function(trip) {
        marker = new window.PruneCluster.Marker(trip.center.lat, trip.center.lng);
        var html = buildMarkerPopUp(trip);
        marker.data.popup = html;
        pruneCluster.RegisterMarker(marker);
      });
      pruneCluster.ProcessView();
    };

    var idOfWorld = '56ad04c50a86a01b071819c7';
    // Get child places and get trips data for them
    vm.childs = [];
    $http.get('api/places/parent/' + idOfWorld).success(function (response) {
      vm.childs = response;
      angular.forEach(vm.childs, function(child) {
        $http.get('api/trips/place/' + child._id).success(function (response2) {
          child.trips = response2;
          makeMarkers(child.trips);
          $scope.sliderPos[child._id] = 0;
          $scope.tripsCount[child._id] = response2.length;
        });
      });
    });

    leafletData.getMap('homeMap').then(function(map) {
      map.addLayer(pruneCluster);
    });
    pruneCluster.ProcessView();
    // Leaflet
    vm.leaflet = {};
    vm.leaflet.defaults = {
      tileLayer: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
      maxZoom: 14,
      scrollWheelZoom: false
    };

  }
]);
