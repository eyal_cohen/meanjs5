'use strict';

angular.module('core').controller('FooterController', ['$scope', '$state', 'Authentication', 'Menus',
  function ($scope, $state, Authentication, Menus) {

    var vm = this;

    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;

    vm.date = new Date();
  }
]);
