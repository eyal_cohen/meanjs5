'use strict';

var validator = require('validator'),
  mongoose = require('mongoose'),
  Trip = mongoose.model('Trip'),
  Place = mongoose.model('Place'),
  User = mongoose.model('User');

/**
 * Render the main application page
 */
exports.renderIndex = function (req, res) {

  var safeUserObject = null;
  if (req.user) {
    safeUserObject = {
      displayName: validator.escape(req.user.displayName),
      provider: validator.escape(req.user.provider),
      username: validator.escape(req.user.username),
      created: req.user.created.toString(),
      roles: req.user.roles,
      profileImageURL: req.user.profileImageURL,
      email: validator.escape(req.user.email),
      lastName: validator.escape(req.user.lastName),
      firstName: validator.escape(req.user.firstName)
    };
  }

  res.render('modules/core/server/views/index', {
    user: safeUserObject
  });
};

/**
 * Render the main application page
 */
exports.renderPage = function (req, res) {

  var safeUserObject = null;
  if (req.user) {
    safeUserObject = {
      displayName: validator.escape(req.user.displayName),
      provider: validator.escape(req.user.provider),
      username: validator.escape(req.user.username),
      created: req.user.created.toString(),
      roles: req.user.roles,
      profileImageURL: req.user.profileImageURL,
      email: validator.escape(req.user.email),
      lastName: validator.escape(req.user.lastName),
      firstName: validator.escape(req.user.firstName)
    };
  }

  var url_slug = req.url.substring(1);

  Trip.findOne({ $and: [{ slug: url_slug }, { status: 'LIVE' }] }).populate('user', 'displayName').populate('users', 'displayName').populate('places.place', 'name').exec(function (err, tmp_trip) {
    if (err) {
      // error
    } else if (!tmp_trip) {
      // maybe it's place page
      console.log('here');
      res.render('modules/core/server/views/index', {
        user: safeUserObject
      });
    } else {  
      // trip found
      res.render('modules/core/server/views/trip', {
        user: safeUserObject,
        trip: tmp_trip
      });
    }
  });

};

/**
 * Render the server error page
 */
exports.renderServerError = function (req, res) {
  res.status(500).render('modules/core/server/views/500', {
    error: 'Oops! Something went wrong...'
  });
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = function (req, res) {

  res.status(404).format({
    'text/html': function () {
      res.render('modules/core/server/views/404', {
        url: req.originalUrl
      });
    },
    'application/json': function () {
      res.json({
        error: 'Path not found'
      });
    },
    'default': function () {
      res.send('Path not found');
    }
  });
};
