'use strict';

/**
 * Module dependencies
 */
var placesPolicy = require('../policies/places.server.policy'),
  places = require('../controllers/places.server.controller');

module.exports = function (app) {
  // Places collection routes
  app.route('/api/places').all(placesPolicy.isAllowed)
    .get(places.list)
    .post(places.create);

  // Single place routes
  app.route('/api/places/:placeId').all(placesPolicy.isAllowed)
    .get(places.read)
    .put(places.update)
    .delete(places.delete);

  app.route('/api/places/parent/:parentId')
    .get(places.read);

  // Finish by binding the place middleware
  app.param('placeId', places.placeByID);

  // Finish by binding the palces middleware
  app.param('parentId', places.placeByParentID);
  
};
