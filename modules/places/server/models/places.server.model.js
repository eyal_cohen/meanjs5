'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Place Schema
 */
var PlaceSchema = new Schema({
  name: {
    type: String,
    default: '',
    trim: true
  },
  alternativeNames: [{
    type: String,
    default: '',
    trim: true
  }],
  created: {
    type: Date,
    default: Date.now
  },
  description: [{
    text: {
      type: String,
      default: '',
      trim: true
    },
    activity: {
      type: Schema.ObjectId,
      ref: 'Activity'
    }
  }],
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  approval: {
    type: Boolean,
    default: false
  },
  parent: {
    type: Schema.ObjectId,
    ref: 'Place'  
  },
  parents: [{
    type: Schema.ObjectId,
    ref: 'Place'  
  }],
  links: [{
    url: String,
    description: String
  }]
});

// PlaceSchema.index({ 'map.features.geometry': '2dsphere' });

mongoose.model('Place', PlaceSchema);
