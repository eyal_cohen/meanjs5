(function (app) {
  'use strict';

  app.registerModule('places');
  app.registerModule('places.services');
  app.registerModule('places.routes', ['ui.router', 'places.services']);
  app.registerModule('places.admin.routes', ['core.admin.routes']);
  app.registerModule('places.admin', ['core.admin']);
})(ApplicationConfiguration);
