'use strict';

// Setting up route
angular.module('places.admin.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('admin.places', {
        url: '/places',
        templateUrl: 'modules/places/client/views/admin/list-places.client.view.html',
        controller: 'AdminPlaceListController',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.place-edit', {
        url: '/places/:placeId/edit',
        templateUrl: 'modules/places/client/views/admin/edit-place.client.view.html',
        controller: 'AdminPlaceController',
        controllerAs: 'vm',
        resolve: {
          placeResolve: getPlace
        },
        data: {
          roles: ['admin']
        }
      })
      .state('admin.places-create', {
        url: '/places/create',
        templateUrl: 'modules/places/client/views/admin/edit-place.client.view.html',
        controller: 'AdminPlaceController',
        controllerAs: 'vm',
        resolve: {
          placeResolve: newPlace
        },
        data: {
          roles: ['admin']
        }
      });
    
    getPlace.$inject = ['$stateParams', 'PlacesService'];

    function getPlace($stateParams, PlacesService) {
      return PlacesService.get({
        placeId: $stateParams.placeId
      }).$promise;
    }

    newPlace.$inject = ['PlacesService'];

    function newPlace(PlacesService) {
      return new PlacesService();
    }
  }

]);
