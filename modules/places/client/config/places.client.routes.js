(function () {
  'use strict';

  angular
    .module('places.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('places', {
        abstract: true,
        url: '/places',
        template: '<ui-view/>'
      })
      .state('places.view', {
        url: '/:placeId',
        templateUrl: 'modules/places/client/views/view-place.client.view.html',
        controller: 'PlacesController',
        controllerAs: 'vm',
        resolve: {
          placeResolve: getPlace
        }
      });
  }

  getPlace.$inject = ['$stateParams', 'PlacesService'];

  function getPlace($stateParams, PlacesService) {
    return PlacesService.get({
      placeId: $stateParams.placeId
    }).$promise;
  }

})();
