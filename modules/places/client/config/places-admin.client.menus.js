'use strict';

// Configuring the Activities module
angular.module('places.admin').run(['Menus',
  function (Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Places',
      state: 'admin.places'
    });
  }
]);