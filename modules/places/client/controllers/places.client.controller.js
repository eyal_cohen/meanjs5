(function () {
  'use strict';

  angular
    .module('places')
    .controller('PlacesController', PlacesController);

  PlacesController.$inject = ['$scope', '$state', '$http', 'placeResolve', 'Authentication'];

  function PlacesController($scope, $state, $http, place, Authentication) {
    var vm = this;

    vm.place = place;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};

    var makeMarkers = function(trips) {
      angular.forEach(trips, function(trip) {
        vm.leaflet.markers[trip._id] = {
          lat: trip.center.lat,
          lng: trip.center.lng,
          message: trip.title,
          focus: false,
          draggable: false
        };
      });
      console.log(vm.leaflet.markers);
    };

    // Get child places and get trips data for them
    vm.childs = [];
    $http.get('api/places/parent/' + place._id).success(function (response) {
      vm.childs = response;
      angular.forEach(vm.childs, function(child) {
        $http.get('api/trips/place/' + child._id).success(function (response) {
          child.trips = response;
          makeMarkers(child.trips);
        });
      });
    });

    // Get trips of this place (not of childs...)
    $http.get('api/trips/place/' + place._id).success(function (response) {
      place.trips = response;
      makeMarkers(place.trips);
    });

    // Leaflet
    vm.leaflet = {};
    vm.leaflet.defaults = {
      scrollWheelZoom: false
    };
    // add markers
    vm.leaflet.markers = {};

  }
})();
