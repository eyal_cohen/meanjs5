(function () {
  'use strict';

  angular
    .module('places')
    .controller('SelectPlaceController', SelectPlaceController);

  SelectPlaceController.$inject = ['$scope', '$state', '$http','PlacesService', 'Authentication'];

  function SelectPlaceController($scope, $state, $http, place, PlacesService, Authentication) {
    
    var vm2 = this;

    vm2.place = $scope.$parent.vm.place; // get place object from parent scope
    vm2.authentication = Authentication;

    var idOfWorld = '56ad04c50a86a01b071819c7';
    var idForChooseOption = -1;

    $scope.placesBlocks = [[]];
    $scope.selectedArray = [];

    $scope.placeChanged = function (key, key2) {
      $scope.selectedArray.splice(key2 + 1, Number.MAX_VALUE);
      $scope.placesBlocks[0].splice(key2 + 1, Number.MAX_VALUE);
      var currentValue = $scope.selectedArray[$scope.selectedArray.length - 1];
      if(currentValue !== idForChooseOption) {
        $scope.getSelectData(key, key2 + 1, currentValue);
      }
      $scope.convertSelectToStorableArray(key);
      //console.log($scope.placesBlocks);
    };

    $scope.addPlaceBlock = function () {
      $scope.getSelectData(0, $scope.placesBlocks[0].length, idOfWorld);
    };

    $scope.convertSelectToStorableArray = function (key) {
      var parents = [idOfWorld].concat($scope.selectedArray);
      if (parents[parents.length - 1] === idForChooseOption) {
        parents.splice(parents.length - 1, Number.MAX_VALUE);
      }
      vm2.place.parents = parents;
      vm2.place.parent = parents[parents.length - 1];
    };

    $scope.convertStorableArrayToSelect = function () {
      var numPlaces = vm2.place.parents.length;
      var place;
      for(var i=1; i < numPlaces; i++) { // i==0 is world, skip it
        place = vm2.place.parents[i];
        $scope.getSelectData(0, i, place._id);
        $scope.selectedArray[i-1] = place._id;
      }
    };

    $scope.getSelectData = function (key, key2, placeId) {
      //console.log("getSelectData(" + key + ", " + key2 + ", " + placeId + ")");
      $http.get('api/places/parent/' + placeId).success(function (response){
        var childs = response;
        var places;
        var name;
        if(key2 === 0) {
          name = '- Continent -';
        } else if (key2 === 1) {
          name = '- Country -';
        } else {
          name = '- Region -';
        }
        places = [{ _id: idForChooseOption, name: name }];
        places = places.concat(childs);
        $scope.placesBlocks[0][key2] = { places: places };
        $scope.selectedArray[$scope.selectedArray.length] = idForChooseOption;
        //console.log("getSelectData(" + key + ", " + key2 + ", " + placeId + ") - DONE");
      });
    };

    if($state.current.name !== 'admin.places-create') { // only for editing
      $scope.convertStorableArrayToSelect();
      //console.log($scope.placesBlocks);
    } else {
      vm2.place.parent = idOfWorld;
    }
    $scope.addPlaceBlock();
  }
})();