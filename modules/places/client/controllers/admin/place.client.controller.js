(function () {
  'use strict';

  angular
    .module('places.admin')
    .controller('AdminPlaceController', AdminPlaceController);

  AdminPlaceController.$inject = ['$scope', '$state', 'placeResolve', 'Authentication'];

  function AdminPlaceController($scope, $state, place, Authentication) {
    var vm = this;

    vm.place = place;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    //vm.place.places = [];
    //vm.place.places.push('56abe378565647ebc9210a31');

    // Remove existing Place
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.place.$remove($state.go('admin.places'));
      }
    }

    // Save Place
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.placeForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.place._id) {
        vm.place.$update(successCallback, errorCallback);
      } else {
        vm.place.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('admin.places');
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }  
    }
  }
})();
