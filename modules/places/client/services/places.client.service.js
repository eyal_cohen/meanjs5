(function () {
  'use strict';

  angular
    .module('places.services')
    .factory('PlacesService', PlacesService);

  PlacesService.$inject = ['$resource'];

  function PlacesService($resource) {
    return $resource('api/places/:placeId', {
      placeId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }

  // PlaceParents service used for communicating with the places REST endpoints
  angular
    .module('places')
    .factory('PlaceParents', ['$resource',
    function ($resource) {
      return $resource('api/places/parent/:parentId', {
        parentId: '@_id'
      });
    }
  ]);


})();

