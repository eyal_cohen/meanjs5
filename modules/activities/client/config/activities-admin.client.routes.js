'use strict';

// Setting up route
angular.module('activities.admin.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('admin.activities', {
        url: '/activities',
        templateUrl: 'modules/activities/client/views/admin/list-activities.client.view.html',
        controller: 'AdminActivityListController',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.activity-edit', {
        url: '/activities/:activityId/edit',
        templateUrl: 'modules/activities/client/views/admin/edit-activity.client.view.html',
        controller: 'AdminActivityController',
        controllerAs: 'vm',
        resolve: {
          activityResolve: getActivity
        },
        data: {
          roles: ['admin']
        }
      })
      .state('admin.activities-create', {
        url: '/activities/create',
        templateUrl: 'modules/activities/client/views/admin/edit-activity.client.view.html',
        controller: 'AdminActivityController',
        controllerAs: 'vm',
        resolve: {
          activityResolve: newActivity
        },
        data: {
          roles: ['admin']
        }
      });
  
    getActivity.$inject = ['$stateParams', 'ActivitiesService'];

    function getActivity($stateParams, ActivitiesService) {
      return ActivitiesService.get({
        activityId: $stateParams.activityId
      }).$promise;
    }

    newActivity.$inject = ['ActivitiesService'];

    function newActivity(ActivitiesService) {
      return new ActivitiesService();
    }
  }

]);
