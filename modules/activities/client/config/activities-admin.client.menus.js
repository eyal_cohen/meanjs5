'use strict';

// Configuring the Activities module
angular.module('activities.admin').run(['Menus',
  function (Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Activities',
      state: 'admin.activities'
    });
  }
]);