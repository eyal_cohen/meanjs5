(function (app) {
  'use strict';

  app.registerModule('activities', ['core']);
  app.registerModule('activities.services');
  app.registerModule('activities.routes', ['ui.router', 'activities.services']);
  app.registerModule('activities.admin.routes', ['core.admin.routes']);
  app.registerModule('activities.admin', ['core.admin']);
})(ApplicationConfiguration);