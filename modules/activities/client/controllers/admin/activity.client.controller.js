(function () {
  'use strict';

  angular
    .module('activities.admin')
    .controller('AdminActivityController', AdminActivityController);

  AdminActivityController.$inject = ['$scope', '$state', 'activityResolve', 'Authentication'];

  function AdminActivityController($scope, $state, activity, Authentication) {
    var vm = this;

    vm.activity = activity;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    //vm.activity.activities = [];
    //vm.activity.activities.push('56abe378565647ebc9210a31');
  
    // Remove existing Activity
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.activity.$remove($state.go('admin.activities'));
      }
    }

    // Save Activity
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.activityForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.activity._id) {
        vm.activity.$update(successCallback, errorCallback);
      } else {
        vm.activity.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        console.log(111);
        $state.go('admin.activities');
      }

      function errorCallback(res) {
        console.log(111);
        vm.error = res.data.message;
      }  
    }
  }
})();
