'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Activity Schema
 */
var ActivitySchema = new Schema({
  title: {
    type: String,
    default: '',
    trim: true
  }
});

mongoose.model('Activity', ActivitySchema);
