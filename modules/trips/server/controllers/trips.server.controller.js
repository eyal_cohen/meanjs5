'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Trip = mongoose.model('Trip'),
  Place = mongoose.model('Place'),
  User = mongoose.model('User'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  request = require('request'),
  async = require('async');

/**
 * Create an trip
 */
exports.create = function (req, res) {
  var trip = new Trip(req.body);
  trip.user = req.user;

  trip.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(trip);
    }
  });
};

/**
 * Show the current trip
 */
exports.read = function (req, res) {
  res.json(req.trip);
};

/**
 * publish the current trip
 */
exports.publish = function (req, res) {

  var trip = req.trip;

  Trip.findOne({ _id: trip._id }, function (err, trip){
    trip.status = 'WAITING_APPROVAL';
    trip.save();
  });
};


/**
 * publish the current trip
 */
exports.adminPublish = function (req, res) {

  var trip = req.trip;

  // remove old live version
  Trip.remove({ $and: [{ parent: trip._id },{ status: 'LIVE' }] }, function(err) {
    if (!err) {  
    }
    else {
    }
  });

  var NUMBER_OF_POINTS_PER_REQUEST = 50;
  var BASE_URL = 'https://maps.googleapis.com/maps/api/elevation/json?locations=';
  var END_URL = '&key=AIzaSyBEokB3GJWR9wn6hHHfH3Y_mbV5XkIdpoc';
  var PRECITION = 6;
  var points = []; // Array of points without elevation data

  // for async elevation data fetch
  var calls = [];

  // loop on all map features and collect pointes without elevation data
  for(var feature in trip.map) {
    if(trip.map[feature].geometry !== undefined) {
      var geometry = trip.map[feature].geometry;
      //console.log(geometry);
      if(geometry.type === 'LineString') {
        for(var i in geometry.coordinates) {
          var point = geometry.coordinates[i];
          if(point.length < 3) {
            // no elevation data for this point
            points.push(point);
          }
        }
      } else if(geometry.type === 'Point') {
        if(geometry.coordinates.length < 3) {
          // no elevation data for this point
          points.push(geometry.coordinates);
        }
      }
    }
  }

  var elevationData = {};

  function getElevationData(tempUrl2, callback) {
    setTimeout(function() {
      request(tempUrl2, function(error, response, body) {
        if(error) {
          callback(error);
        } else {
          body = JSON.parse(body);
          for(var h in body.results) {
            var tempPoint = body.results[h];
            //console.log(h);
            var mapKey0 = parseFloat(tempPoint.location.lng).toFixed(PRECITION);
            var mapKey1 = parseFloat(tempPoint.location.lat).toFixed(PRECITION);
            if(Object.prototype.toString.call(elevationData[mapKey0]) !== '[object Array]') {
              elevationData[mapKey0] = [];
            }
            elevationData[mapKey0][mapKey1] = tempPoint.elevation;
          }
          callback(null, body);
        }
      });
    }, 101); // timeout becouse google allows maximum 10 calls per second
  }
  var tempUrl;
  // If there are points without elevation
  if(points.length !== 0) {
    
    // make requests to google elevation api
    var url = BASE_URL;
    var cfunc = function(tempUrl2) {
      return function(callback) { 
        getElevationData(tempUrl2, callback);
      };
    };
    var k;
    for(k = 0; k < points.length; k++) {
      url += points[k][1] + ',' + points[k][0] + '|';
      if((k + 1) % NUMBER_OF_POINTS_PER_REQUEST === 0) {
        url = url.slice(0, -1); // remove last |
        url += END_URL;

        
        tempUrl = url.slice(0);
        calls.push(cfunc(tempUrl));
        url = BASE_URL;
      }
    }
    if(k % NUMBER_OF_POINTS_PER_REQUEST !== 0) {
      url = url.slice(0, -1); // remove last |
      url += END_URL;

      tempUrl = url.slice(0);
      calls.push(cfunc(tempUrl));
    }
  }
  console.log('here1');
  async.series(calls, function(err, result) {
    console.log('here i am');
    console.log(elevationData);
    var mapKey0, mapKey1, elevation;
    for(var feature in trip.map) {
      if(trip.map[feature].geometry !== undefined) {
        var geometry = trip.map[feature].geometry;
        //console.log(geometry);
        if(geometry.type === 'LineString') {
          for(var i in geometry.coordinates) {
            var point = geometry.coordinates[i];
            if(point.length < 3) {
              // no elevation data for this point
              mapKey0 = parseFloat(point[0]).toFixed(PRECITION);
              mapKey1 = parseFloat(point[1]).toFixed(PRECITION);
              if(Object.prototype.toString.call(elevationData[mapKey0]) === '[object Array]') {
                elevation = elevationData[mapKey0][mapKey1];
                if(elevation !== undefined) {
                  geometry.coordinates[i].push(elevation);
                }
              }
            }
          }
        } else if(geometry.type === 'Point') {
          if(geometry.coordinates.length < 3) {
            // no elevation data for this point
            mapKey0 = parseFloat(geometry.coordinates[0]).toFixed(PRECITION);
            mapKey1 = parseFloat(geometry.coordinates[1]).toFixed(PRECITION);
            if(Object.prototype.toString.call(elevationData[mapKey0]) === '[object Array]') {
              elevation = elevationData[mapKey0][mapKey1];
              if(elevation !== undefined) {
                geometry.coordinates.push(elevation);
              }
            }
          }
        }
      }
    }

    console.log(trip.map);

    // update trips
    trip.status = 'PUBLISHED';
    trip.markModified('map');
    trip.save(function (err) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        console.log('done1!');

        // create live version
        trip.parent = trip._id;
        trip._id = mongoose.Types.ObjectId();
        trip.status = 'LIVE';
        trip.isNew = true;
        trip.save(function (err) {
          if (err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          } else {
            console.log('done2!');
            res.json(trip);
          }
        });

      }
    });
  });

  

};

/**
 * publish the current trip
 */
exports.adminUnPublish = function (req, res) {

  var trip = req.trip;

  Trip.findOne({ _id: trip._id }, function (err, parent){
    parent.status = 'DRAFT';
    parent.save();
  });

  Trip.remove({ $and: [{ parent: trip._id },{ status: 'LIVE' }] }, function(err) {
    if (!err) {
      res.json();
    }
    else {
      
    }
  });
};

/**
 * Update an trip
 */
exports.update = function (req, res) {
  var trip = req.trip;

  trip.title = req.body.title;
  trip.subtitle = req.body.subtitle;
  trip.tips = req.body.tips;
  trip.links = req.body.links;
  trip.files = req.body.files;
  trip.chapters = req.body.chapters;
  trip.places = req.body.places;
  trip.map = req.body.map;
  trip.center = req.body.center;
  trip.activities = req.body.activities;
  trip.tripDurationDays = req.body.tripDurationDays;
  trip.tripDurationMonths = req.body.tripDurationMonths;
  trip.tripStart = req.body.tripStart;
  trip.users = req.body.users;
  trip.slug = req.body.slug;
  trip.themeImage = req.body.themeImage;

  trip.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(trip);
    }
  });
};

/**
 * Delete an trip
 */
exports.delete = function (req, res) {
  var trip = req.trip;

  trip.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(trip);
    }
  });
};

/**
 * List of Trips
 */
exports.list = function (req, res) {
  Trip.find({ status: 'ORIGINAL' }).sort('-created').populate('user', 'displayName').exec(function (err, trips) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(trips);
    }
  });
};

/**
 * Trip middleware
 */
exports.tripByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Trip is invalid'
    });
  }

  Trip.findById(id).populate('user', 'displayName').populate('users', 'displayName').populate('places.place', 'name').exec(function (err, trip) {
    if (err) {
      return next(err);
    } else if (!trip) {
      return res.status(404).send({
        message: 'No trip with that identifier has been found'
      });
    }
    req.trip = trip;
    next();
  });
};

exports.tripsByPlace = function (req, res, next, id) {
  // The $elemMatch is used for searching inside array.
  Trip.find({ $and: [{ places: { $elemMatch: { place: { $in:[id] } } } }, { status: 'LIVE' }] }, 'title subtitle places user center themeImage slug').populate('user', 'displayName').lean().exec(function (err, trip) {
    if (err) {
      return next(err);
    } else if (!trip) {
      return res.status(404).send({
        message: 'No trip with that identifier has been found'
      });
    }
    req.trip = trip;
    next();
  });
};

exports.tripsByParent = function (req, res, next, id) {
  // The $elemMatch is used for searching inside array.
  Trip.find({ parent: id }, 'title subtitle places user center').populate('user', 'displayName').lean().exec(function (err, trip) {
    if (err) {
      return next(err);
    } else if (!trip) {
      return res.status(404).send({
        message: 'No trip with that identifier has been found'
      });
    }
    req.trip = trip;
    next();
  });
};

exports.tripsByStatus = function (req, res, next, id) {
  // The $elemMatch is used for searching inside array.
  Trip.find({ status: id }, 'title user created').sort('-created').populate('user', 'displayName').lean().exec(function (err, trip) {
    if (err) {
      return next(err);
    } else if (!trip) {
      return res.status(404).send({
        message: 'No trips with that identifier has been found'
      });
    }
    req.trip = trip;
    next();
  });
};