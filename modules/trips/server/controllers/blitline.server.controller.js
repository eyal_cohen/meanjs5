'use strict';

/**
 * Module dependencies
 */
var Blitline = require('simple_blitline_node');
var nodeutil = require('util');
var blitline = new Blitline();

var applicationID = '4c6wAlWKtaSfxBt-leO7gQA';

exports.job = function(req, res) {
  blitline.addJob(req.body);
  blitline.postJobs(function(response) {
    console.log(nodeutil.inspect(response, { depth:10, colors: true }));
  });
  res.jsonp({});
};
