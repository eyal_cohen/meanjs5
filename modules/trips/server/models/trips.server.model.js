'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Trip Schema
 */
var TripSchema = new Schema({
  status: {
    type: String,
    default: 'DRAFT',
    trim: true
  },
  parent: {
    type: Schema.ObjectId,
    ref: 'Trip'  
  },
  title: {
    type: String,
    default: '',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  lastUpdate: {
    type: Date,
    default: Date.now
  },
  subtitle: {
    type: String,
    default: '',
    trim: true
  },
  slug: {
    type: String,
    default: '',
    trim: true
  },
  activities: [{
    type: Schema.ObjectId,
    ref: 'Activity'    
  }],
  places: [{
    place: [{
      type: Schema.ObjectId,
      ref: 'Place' 
    }]
  }],
  bestSeason: {
    type: String,
    default: '',
    trim: true
  },
  tripStart: {
    type: Date,
    default: Date.now
  },
  tripDurationDays: {
    type: Number,
    defualt: 0
  },
  tripDurationMonths: {
    type: Number,
    defualt: 0
  },
  views: {
    type: Number,
    defualt: 0
  },
  chapters: [{
    title: String,
    body: String,
    images: [{
      file: String,
      description: String
    }],
    videos: [{
      url: String
    }]
  }],
  map: [
    {
      type: {
        type: String
      },
      properties: {},
      geometry: {
        type: {
          type: String
        },
        coordinates: []
      }
    }
  ],
  center: {
    lat: Number,
    lng: Number
  },
  links: [{
    url: String,
    description: String
  }],
  files: [{
    filename: String,
    server_filename: String,
    filetype: String,
    description: String
  }],
  tips: [{
    body: String
  }],
  themeImage: {
    type: String,
    default: ''
  },
  comments: [{
    user: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    body: String,
    created: {
      type: Date,
      default: Date.now
    }
  }],
  users: [{
    type: Schema.ObjectId,
    ref: 'User'
  }]
});

//TripSchema.index({ 'map.geometry': '2dsphere' });

mongoose.model('Trip', TripSchema);
