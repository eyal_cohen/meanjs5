'use strict';

/**
 * Module dependencies
 */
var tripsPolicy = require('../policies/trips.server.policy'),
  trips = require('../controllers/trips.server.controller'),
  aws = require('../controllers/aws-signing.server.controller'),
  blitline = require('../controllers/blitline.server.controller');

module.exports = function (app) {
  // Trips collection routes
  app.route('/api/trips').all(tripsPolicy.isAllowed)
    .get(trips.list)
    .post(trips.create);

  // Trips collection routes
  app.route('/api/trips/status/:status').all(tripsPolicy.isAllowed)
    .get(trips.read);

  // Trips collection routes
  app.route('/api/trips/parent/:parentId').get(trips.read);

  // Single trip routes
  app.route('/api/trips/:tripId').all(tripsPolicy.isAllowed)
    .get(trips.read)
    .put(trips.update)
    .delete(trips.delete);

  // Trips collection routes
  app.route('/api/trips/place/:placeId').all(tripsPolicy.isAllowed)
    .get(trips.read);

  // Approve trip
  app.route('/admin/trips/:tripId/publish').post(trips.adminPublish);

  // Approve trip
  app.route('/admin/trips/:tripId/unpublish').post(trips.adminUnPublish);

  // Send trip for approval
  app.route('/trips/:tripId/publish').post(trips.publish);

  // Amazon aws signin route
  app.route('/aws-signing').post(aws.signing);

  // Blitline job route
  app.route('/blitline').post(blitline.job);

  // Finish by binding the trip middleware
  app.param('tripId', trips.tripByID);
  app.param('placeId', trips.tripsByPlace);
  app.param('parentId', trips.tripsByParent);
  app.param('status', trips.tripsByStatus);
};
