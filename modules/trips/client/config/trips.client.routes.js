(function () {
  'use strict';

  angular
    .module('trips.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('trips', {
        abstract: true,
        url: '/trips',
        template: '<ui-view/>'
      })
      .state('trips.list', {
        url: '',
        templateUrl: 'modules/trips/client/views/list-trips.client.view.html',
        controller: 'TripsListController',
        controllerAs: 'vm'
      })
      .state('trips.create', {
        url: '/create',
        controller: 'CreateTripController',
        controllerAs: 'vm',
        resolve: {
          tripResolve: newTrip
        },
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('editor', {
        url: '/trips/:tripId/edit',
        templateUrl: 'modules/trips/client/views/editor/trip-editor.client.view.html',
        controller: 'EditorController',
        controllerAs: 'vm',
        resolve: {
          tripResolve: getTrip,
          tripResolve2: getTrip // another copy of trip for the editor controller
        },
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('editor.titles', {
        url: '/titles',
        templateUrl: 'modules/trips/client/views/editor/titles.client.view.html'
      })
      .state('editor.general-details', {
        url: '/general-details',
        templateUrl: 'modules/trips/client/views/editor/general-details.client.view.html',
        controller: 'EditorGeneralDetailsController',
        controllerAs: 'vm2',
      })
      .state('editor.chapters', {
        url: '/chapters/:chapterId',
        templateUrl: 'modules/trips/client/views/editor/chapters.client.view.html',
        controller: 'EditorChaptersController',
        controllerAs: 'vm2',
      })
      .state('editor.map', {
        url: '/map',
        templateUrl: 'modules/trips/client/views/editor/map.client.view.html',
        controller: 'EditorMapController',
        controllerAs: 'vm2',
      })
      .state('editor.links', {
        url: '/links',
        templateUrl: 'modules/trips/client/views/editor/links.client.view.html',
        controller: 'EditorLinksController',
        controllerAs: 'vm2',
      })
      .state('editor.files', {
        url: '/files',
        templateUrl: 'modules/trips/client/views/editor/files.client.view.html',
        controller: 'EditorFilesController',
        controllerAs: 'vm2',
      })
      .state('editor.tips', {
        url: '/tips',
        templateUrl: 'modules/trips/client/views/editor/tips.client.view.html',
        controller: 'EditorTipsController',
        controllerAs: 'vm2',
      })
      .state('editor.friends', {
        url: '/friends',
        templateUrl: 'modules/trips/client/views/editor/friends.client.view.html',
        controller: 'EditorFriendsController',
        controllerAs: 'vm2'
      })
      .state('editor.preview', {
        url: '/preview',
        templateUrl: 'modules/trips/client/views/editor/preview.client.view.html',
        controller: 'EditorPreviewController',
        controllerAs: 'vm2',
      })
      .state('trips.view', {
        url: '/:tripId',
        templateUrl: 'modules/trips/client/views/view-trip.client.view.html',
        controller: 'TripsController',
        controllerAs: 'vm',
        resolve: {
          tripResolve: getTrip
        }
      });
  }

  getTrip.$inject = ['$stateParams', 'TripsService'];

  function getTrip($stateParams, TripsService) {
    return TripsService.get({
      tripId: $stateParams.tripId
    }).$promise;
  }

  newTrip.$inject = ['TripsService'];

  function newTrip(TripsService) {
    return new TripsService();
  }
})();
