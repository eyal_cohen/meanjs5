'use strict';

// Configuring the Trips module
angular.module('trips.admin').run(['Menus',
  function (Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Trips',
      state: 'admin.trips'
    });
  }
]);