'use strict';

// Setting up route
angular.module('trips.admin.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('admin.trips', {
        url: '/trips',
        templateUrl: 'modules/trips/client/views/admin/list-trips.client.view.html',
        controller: 'AdminTripListController',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.trip-edit', {
        url: '/trips/:tripId/edit',
        templateUrl: 'modules/trips/client/views/admin/edit-trip.client.view.html',
        controller: 'AdminTripController',
        controllerAs: 'vm',
        resolve: {
          tripResolve: getTrip
        },
        data: {
          roles: ['admin']
        }
      })
      .state('admin.trips-create', {
        url: '/trips/create',
        templateUrl: 'modules/trips/client/views/admin/edit-trip.client.view.html',
        controller: 'AdminTripController',
        controllerAs: 'vm',
        resolve: {
          tripResolve: newTrip
        },
        data: {
          roles: ['admin']
        }
      });
  
    getTrip.$inject = ['$stateParams', 'TripsService'];

    function getTrip($stateParams, TripsService) {
      return TripsService.get({
        tripId: $stateParams.tripId
      }).$promise;
    }

    newTrip.$inject = ['TripsService'];

    function newTrip(TripsService) {
      return new TripsService();
    }
  }

]);
