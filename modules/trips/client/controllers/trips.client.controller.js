(function () {
  'use strict';

  angular
    .module('trips')
    .controller('TripsController', TripsController);

  TripsController.$inject = ['$scope', '$state', 'tripResolve', 'leafletData', 'Authentication'];

  function TripsController($scope, $state, trip, leafletData, Authentication) {
    var vm = this;

    vm.trip = trip;
    vm.authentication = Authentication;

    console.log(vm.trip);

    // Leaflet
    vm.leaflet = {};
    vm.leaflet.defaults = {
      tileLayer: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
      maxZoom: 14,
      scrollWheelZoom: false,

    };


    // =========
    // = Icons =
    // =========
    var MapIcon = window.L.Icon.extend({
      options: {
        shadowUrl: '',
        iconSize:     [29, 36],
        shadowSize:   [0, 0],
        iconAnchor:   [14, 38],
        shadowAnchor: [0, 0],
        popupAnchor:  [0, -35]
      }
    });

    // marker colors
    $scope.iconColors = [
      {
        color: 'Black'
      },
      {
        color: 'Blue'
      },
      {
        color: 'Green'
      },
      {
        color: 'Purpel'
      },
      {
        color: 'Red'
      }
    ];

    // marker icons
    $scope.iconTypes = [
      {
        name: 'Marker',
        file: 'marker'
      },
      {
        name: 'Bridge',
        file: 'bridge'
      },
      {
        name: 'Dive Site',
        file: 'dive-site'
      },
      {
        name: 'Fish',
        file: 'fish'
      },
      {
        name: 'Flag',
        file: 'flag'
      },
      {
        name: 'Food',
        file: 'food'
      },
      {
        name: 'Museum',
        file: 'museum'
      },
      {
        name: 'Hotel',
        file: 'hotel'
      },
      {
        name: 'Turist information',
        file: 'info'
      },
      {
        name: 'Pass / Saddle',
        file: 'pass'
      },
      {
        name: 'Peak / Mountain',
        file: 'peak'
      },
      {
        name: 'Attraction',
        file: 'star'
      },
      {
        name: 'Tent',
        file: 'tent'
      },
      {
        name: 'Water',
        file: 'water'
      },
      {
        name: 'Wreck',
        file: 'wreck'
      },
      {
        name: 'Danger',
        file: 'danger'
      }
    ];

    var iconsPath = 'modules/trips/client/img/map/markers/';

    $scope.icons = [];
    var iconType;
    var colors;
    for(var i in $scope.iconTypes) {
      iconType = $scope.iconTypes[i];
      colors = [];
      for(var j in $scope.iconColors) {
        colors[$scope.iconColors[j].color] = new MapIcon({ iconUrl: iconsPath + $scope.iconColors[j].color.toLowerCase() + '/' + iconType.file + '.png' });
      }
      $scope.icons[iconType.file] = {
        name: iconType.name,
        color: colors
      };
    }

    //all used options are the default values
    var el = window.L.control.elevation({
      position: 'bottomleft',
      theme: 'lime-theme', //optional: steelblue-theme
      width: 300,
      height: 125,
      margins: {
        top: 10,
        right: 20,
        bottom: 30,
        left: 40
      },
      useHeightIndicator: true, //if false a marker is drawn at map position
      interpolation: 'linear', //see https://github.com/mbostock/d3/wiki/SVG-Shapes#wiki-area_interpolate
      hoverNumber: {
        decimalsX: 3, //decimals on distance (always in km)
        decimalsY: 0, //deciamls on hehttps://www.npmjs.com/package/leaflet.coordinatesight (always in m)
        formatter: undefined //custom formatter function may be injected
      },
      xTicks: undefined, //number of ticks in x axis, calculated by default according to width
      yTicks: undefined, //number of ticks on y axis, calculated by default according to height
      collapsed: false,  //collapsed mode, show chart on click or mouseover
      imperial: false    //display imperial units instead of metric
    });

    var json_data = {
      type: 'FeatureCollection',
      features: trip.map
    };

    function style(feature) {
      //console.log(feature);
      return {
        color: feature.properties.color,
        opacity: 1,
        weight: 4,
        clickable: true
      };
    }

    function pathClick(leafletEvent) {
    }

    leafletData.getMap('map1').then(function(map) {
      el.addTo(map);
      el.hide();
      var control = window.L.Control.Fullscreen;
      control.addTo(map);
    });

    $scope.shownGraph = -1;

    $scope.showGraph = function(id) {
      if($scope.shownGraph !== id) {
        $scope.centerOnPath(id);
        el.show();
        el.clear();
        window.L.geoJson(trip.map[id], {
          style: { opacity: 0 },
          onEachFeature: el.addData.bind(el) //working on a better solution
        });
        $scope.shownGraph = id;
      } else {
        el.hide();
        $scope.shownGraph = -1;
      }
      
    };

    $scope.geojson = {
      data: json_data,
      onEachFeature: function (feature, layer) {
        if(feature.geometry.type === 'LineString') {
          layer.setStyle(style(feature));
          /*
          layer.on({
            click: pathClick
          });
          */
          if(feature.properties.description !== '') {
            layer.bindPopup(feature.properties.description);
          }
        } else if(feature.geometry.type === 'Point') {
          //console.log(feature);
          layer.setIcon($scope.icons[feature.properties.icon].color[feature.properties.color]);
          var text = '<p style="margin: 5px; text-align: center;">' +
            feature.properties.description +
            '</p><p style="margin: 5px; text-align: center;"><span class="small">Alt: ' +
            parseFloat(feature.geometry.coordinates[2]).toFixed(0) + 'm</span></p>';
          layer.bindPopup(text);
          
        }
      }
    };

    $scope.centerJSON = function() {
      leafletData.getMap('map1').then(function(map) {
        var latlngs = [];
        for(var id in $scope.geojson.data.features) {
          var coordinates = $scope.geojson.data.features[id].geometry.coordinates;
          if($scope.geojson.data.features[id].geometry.type === 'LineString') {
            for (var i in coordinates) {
              latlngs.push(window.L.GeoJSON.coordsToLatLng(coordinates[i]));
            }
          } else if($scope.geojson.data.features[id].geometry.type === 'Point') {
            latlngs.push(window.L.GeoJSON.coordsToLatLng(coordinates));
          }
        }
        map.fitBounds(latlngs);
      });
    };

    $scope.centerOnPath = function(id) {
      el.hide();
      $scope.shownGraph = -1;
      leafletData.getMap('map1').then(function(map) {
        var coordinates = $scope.geojson.data.features[id].geometry.coordinates;
        var latlngs = [];
        for (var i in coordinates) {
          latlngs.push(window.L.GeoJSON.coordsToLatLng(coordinates[i]));
        }
        map.fitBounds(latlngs);
      });
    };

    $scope.centerJSON();

  }
})();
