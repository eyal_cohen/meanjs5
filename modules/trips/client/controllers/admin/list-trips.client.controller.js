'use strict';

angular.module('trips.admin').controller('AdminTripListController', ['$scope', '$filter', '$http',
  function ($scope, $filter, $http) {

    // Fetch WAITING_APPROVAL
    $http.get('api/trips/status/WAITING_APPROVAL')
      .success(function(data, status, headers, config) {
        $scope.waiting_approval_trips = data;
      }).
      error(function(data, status, headers, config) {
      });

    // Fetch DRAFT
    $http.get('api/trips/status/DRAFT')
      .success(function(data, status, headers, config) {
        $scope.draft_trips = data;
      }).
      error(function(data, status, headers, config) {
      });

    // Fetch PUBLISHED
    $http.get('api/trips/status/PUBLISHED')
      .success(function(data, status, headers, config) {
        $scope.approved_trips = data;
      }).
      error(function(data, status, headers, config) {
      });
  }
]);
