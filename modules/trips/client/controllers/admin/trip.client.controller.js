(function () {
  'use strict';

  angular
    .module('trips.admin')
    .controller('AdminTripController', AdminTripController);

  AdminTripController.$inject = ['$scope', '$state', '$http', 'tripResolve', 'Authentication'];

  function AdminTripController($scope, $state, $http, trip, Authentication) {
    var vm = this;

    vm.trip = trip;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};

    if(trip.slug === '') {
      if(trip.places instanceof Array) {
        if(trip.places.length > 0) {
          if(trip.places[0].place instanceof Array) {
            if(trip.places[0].place.length > 2) {
              var place = trip.places[0].place[2];
              $http.get('api/places/' + place)
                .success(function(data, status, headers, config) {
                  trip.slug = convertToSlug(data.name) + '/';
                }).
                error(function(data, status, headers, config) {
                });
            }
          }
        }
      }
    }

    function convertToSlug(slug)
    {
      return slug
          .toLowerCase()
          .replace(/[^\w ]+/g,'')
          .replace(/ +/g,'-');
    }

    // Save Trip
    $scope.save = function() {
      console.log('saving');
      vm.trip.$update(function(response) { console.log(response); }, function() { console.log('error'); });
    };

    $scope.isPublished = false;
    $http.get('api/trips/parent/' + trip._id)
      .success(function(data, status, headers, config) {
        if(data.length > 0) {
          $scope.isPublished = true;
        }
      }).
      error(function(data, status, headers, config) {
      });

    // Remove existing Trip
    $scope.remove = function() {
      if (confirm('Are you sure you want to delete?')) {
        vm.trip.$remove($state.go('admin.trips'));
      }
    };

    $scope.publish = function() {
      $http.post('admin/trips/' + trip._id + '/publish', null).then(function(response) {
        $scope.isPublished = true;
      }, function(response) {
      });
    };

    $scope.unpublish = function() {
      $http.post('admin/trips/' + trip._id + '/unpublish', null).then(function(response) {
        $scope.isPublished = false;
      }, function(response) {
      });
    };

  }
})();
