(function () {
  'use strict';

  angular
    .module('trips')
    .controller('EditorController', EditorController);

  EditorController.$inject = ['$scope', '$state', '$timeout', '$http', '$sce', 'tripResolve', 'tripResolve2', 'Authentication'];

  function EditorController($scope, $state, $timeout, $http, $sce, trip, trip2, Authentication) {
    var vm = this;

    vm.trip = trip;
    $scope.trip_client = trip2; // another copy of trip
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    $scope.lastEditedChapter = 1;
    if($state.current.name === 'editor.chapters') {
      //vm.lastEditedChapter = $state.params.chapterId;
    }

    $scope.deleteTrip = function() {
      if(confirm('Are you sure you want to delete this trip?')) {
        $http.delete('/api/trips/' + trip._id, vm.trip, null).then(function(response) {
          window.location = '/';
        }, function(response) {
          // error
        });
      }
    };

    $scope.publish = function() {
      $http.post('/trips/' + trip._id + '/publish', null).then(function(response) {
        console.log(response);
      }, function(response) {
        // error
      });
    };


    // *************
    // ** Friends **
    // *************
    $scope.selected_friends = [];
    $scope.friends = [];
    $scope.friendsFirstLoad = true;


    // ****************
    // ** Activities **
    // ****************

    // get activities names from server
    $scope.activities = [];
    $http.get('api/activities').success(function (response) {
      $scope.activities = response;
    });
    
    // ************
    // ** Places **
    // ************

    $scope.placesBlocks = [];
    $scope.selectedArray = [];
    $scope.newPlacesArray = [];
    $scope.placesFirstLoad = true;


    // ******************
    // ** File Uploads **
    // ******************

    $scope.chapter_images = [];
    $scope.chapter_images_loaded_flag = 0;

    $scope.files_array = [];

    function restoreFiles() {
      angular.forEach($scope.trip_client.files, function(file, key) {
        $scope.files_array.push({
          filename: file.filename,
          server_filename: file.server_filename,
          filetype: file.filetype,
          description: file.description,
          progress: 100,
          typeimg: 'modules/trips/client/img/editor/filetypes/'+ file.filetype +'.png'
        });
      });
    }

    if($scope.trip_client.files.length !== 0) {
      console.log('restore_files');
      restoreFiles();
    }

    // *************
    // ** Leaflet **
    // *************
    $scope.leaflet = {};
    angular.extend($scope.leaflet, {
      center: {
        lat: 52.374004,
        lng: 4.890359,
        zoom: 7
      },
      defaults: {
        scrollWheelZoom: false
      },
      events: {
        map: {
          enable: ['zoomstart', 'drag', 'click', 'mousemove', 'moveend'],
          logic: 'emit'
        }
      }
    });

    // *************
    // ** Preview **
    // *************
    vm.previewBtn = {
      state: false,
      label: 'Preview',
      icon: 'eye-open'
    };
    vm.lastState = '';
    vm.togglePreview = function() {
      if($state.current.name === 'editor.preview') {
        vm.previewBtn.state = false;
        vm.previewBtn.label = 'Preview';
        vm.previewBtn.icon = 'eye-open';
        if(vm.lastState !== '') {
          if(vm.lastState === 'editor.chapters') {
            console.log('here');
            $state.go('editor.chapters', { chapterId: 1 });
          }
          $state.go(vm.lastState);
        } else {
          $state.go('editor.titles');
        }
      } else {
        vm.previewBtn.state = true;
        vm.previewBtn.label = 'Edit';
        vm.previewBtn.icon = 'edit';
        vm.lastState = $state.current.name;
        $state.go('editor.preview');
      }
    };

    // ***************
    // ** Auto save **
    // ***************

    $scope.logScope = function() {
      console.log(vm);
    };

    // init vars
    var INTERVAL = 800; // 1000 = 1 second
    var saveConsoleMsg = {
      saving: 'Saving...',
      done: 'All changes saved in server',
      error: 'Saving failed. Can\'t find server.'
    };
    var timeout = null;
    var firstSave = 1;

    // do actual save
    var saveUpdates = function() {
      save(true);
    };

    // save helper with interval
    var debounceSaveUpdates = function(newVal, oldVal) {
      if(newVal === undefined) return; // on page load newVal is undefined.
      //console.log(newVal, oldVal);
      if(firstSave > 4) {
        if (newVal !== oldVal) {
          if (timeout) {
            $timeout.cancel(timeout);
          }
          // update trip by trip_client
          vm.trip.title = $scope.trip_client.title;
          vm.trip.subtitle = $scope.trip_client.subtitle;
          vm.trip.tips = $scope.trip_client.tips;
          vm.trip.links = $scope.trip_client.links;
          vm.trip.files = $scope.trip_client.files;
          vm.trip.chapters = $scope.trip_client.chapters;
          vm.trip.places = $scope.trip_client.places;
          vm.trip.map = $scope.trip_client.map;
          vm.trip.center = $scope.trip_client.center;
          vm.trip.activities = $scope.trip_client.activities;
          vm.trip.tripDurationDays = $scope.trip_client.tripDurationDays;
          vm.trip.tripDurationMonths = $scope.trip_client.tripDurationMonths;
          vm.trip.tripStart = $scope.trip_client.tripStart;
          vm.trip.users = $scope.trip_client.users;

          vm.saveConsole = $sce.trustAsHtml(saveConsoleMsg.saving);
          timeout = $timeout(saveUpdates, INTERVAL);
        }
      } else {
        firstSave++;
      }
    };

    // *************
    // ** Actions **
    // *************

    // Remove existing Trip
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.trip.$remove($state.go('trips.list'));
      }
    }

    // Save Trip
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.tripForm');
        return false;
      }
      console.log(trip);
      // TODO: move create/update logic to service
      if (vm.trip._id) {
        vm.trip.$update(successCallback, errorCallback);
      } else {
        vm.trip.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        vm.saveConsole = $sce.trustAsHtml(saveConsoleMsg.done);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
        vm.saveConsole = $sce.trustAsHtml(saveConsoleMsg.error + ' ' + res.data.message);
      }
    }

    // ***********************
    // ** Watch for changes **
    // ***********************
    $scope.$watch('trip_client.title', debounceSaveUpdates);
    $scope.$watch('trip_client.subtitle', debounceSaveUpdates);
    $scope.$watch('trip_client.tips', debounceSaveUpdates, true);
    $scope.$watch('trip_client.files', debounceSaveUpdates, true);
    $scope.$watch('trip_client.links', debounceSaveUpdates, true);
    $scope.$watch('trip_client.chapters', debounceSaveUpdates, true);
    $scope.$watch('trip_client.map', debounceSaveUpdates, true);
    $scope.$watch('trip_client.places', debounceSaveUpdates, true);
    $scope.$watch('trip_client.activities', debounceSaveUpdates, true);
    $scope.$watch('trip_client.tripDurationDays', debounceSaveUpdates);
    $scope.$watch('trip_client.tripDurationMonths', debounceSaveUpdates);
    $scope.$watch('trip_client.tripStart', debounceSaveUpdates);
    $scope.$watch('trip_client.users', debounceSaveUpdates, true);


    
  }
})();
