(function () {
  'use strict';

  angular
    .module('trips')
    .controller('EditorFriendsController', EditorFriendsController);

  EditorFriendsController.$inject = ['$scope', '$state', '$timeout', '$http', '$sce', 'Authentication'];

  function EditorFriendsController($scope, $state, $timeout, $http, $sce, Authentication) {

    var vm = $scope.$parent;
    var vm2 = this;

    vm2.parent = vm;

    $scope.search_friend = '';

    $scope.findUsers = function() {
      $http.get('api/users/by-name/' + $scope.search_friend).success(function (response) {
        console.log(response);
        vm.friends = response;
      });
    };

    $scope.$watch('search_friend', function() {
      if($scope.search_friend.length >= 3) {
        $scope.findUsers();
      } else {
        vm.friends = [];
      }
    });

    if(vm.friendsFirstLoad) {
      angular.forEach(vm.trip_client.users, function(value, key) {
        vm.selected_friends.push({
          _id: value._id,
          displayName: value.displayName
        });
      });
      vm.friendsFirstLoad = false;
    }

    $scope.addFriend = function(id) {
      console.log(id);
      var flag = false;
      angular.forEach(vm.selected_friends, function(value, key) {
        if(value._id === id) {
          flag = true;
          //vm.selected_friends.splice(key ,1);
        }
      });
      if(!flag) {
        angular.forEach(vm.friends, function(value, key) {
          console.log(value._id);
          if(value._id === id) {
            vm.selected_friends.push({
              _id: value._id,
              displayName: value.displayName
            });
            vm.trip_client.users.push(value._id);
            $scope.search_friend = '';
            vm.friends = [];
          }
        });
        //console.log(vm.selected_friends);
      }
    };

    $scope.deleteFriend = function(key) {
      vm.selected_friends.splice(key ,1);
      vm.trip_client.users.splice(key,1);
    };

  }
})();
