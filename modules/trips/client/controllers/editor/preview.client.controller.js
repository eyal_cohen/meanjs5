(function () {
  'use strict';

  angular
    .module('trips')
    .controller('EditorPreviewController', EditorPreviewController);

  EditorPreviewController.$inject = ['$scope', '$state', '$timeout', '$sce', 'Authentication'];

  function EditorPreviewController($scope, $state, $timeout, $sce, Authentication) {

    var vm2 = this;
    vm2.parent = $scope.$parent;

    vm2.leaflet = vm2.parent.leaflet;

  }

})();
