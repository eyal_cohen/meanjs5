(function () {
  'use strict';

  angular
    .module('trips')
    .controller('EditorGeneralDetailsController', EditorGeneralDetailsController);

  EditorGeneralDetailsController.$inject = ['$scope', '$state', '$http', '$timeout', '$sce', 'Authentication', 'PlacesService'];

  function EditorGeneralDetailsController($scope, $state, $http, $timeout, $sce, Authentication, PlacesService) {

    var vm2 = this;
    vm2.parent = $scope.$parent;

    var vm = vm2.parent;

    var idOfWorld = '56ad04c50a86a01b071819c7';
    var idForChooseOption = -1;
    var idForOtherOption = -2;

    /**************/
    /* Activities */
    /**************/

    // restore checked activities
    $scope.selected_activities = { ids: {} };
    angular.forEach(vm.trip_client.activities, function(value, key) {
      $scope.selected_activities.ids[value] = true;
    });

    // update client trip after change
    vm2.updateActivitiesArray = function() {
      var activities = [];
      angular.forEach($scope.selected_activities.ids, function(value, key) {
        if(value) {
          activities.push(key);     
        }
      });
      vm.trip_client.activities = activities;
    };

    $scope.$watch('selected_activities', function() {
      $timeout(vm2.updateActivitiesArray, 200);
    }, true);

    /*********/
    /* Dates */
    /*********/

    $scope.years = [];
    var year = new Date().getFullYear();
    for(var y = year - 6; y <=year; y++) {
      $scope.years.push({ year: y });
    }

    $scope.months = [{
      id: 0,
      label: 'January'
    },{
      id: 1,
      label: 'February'
    },{
      id: 2,
      label: 'March'
    },{
      id: 3,
      label: 'April'
    },{
      id: 4,
      label: 'May'
    },{
      id: 5,
      label: 'June'
    },{
      id: 6,
      label: 'July'
    },{
      id: 7,
      label: 'August'
    },{
      id: 8,
      label: 'September'
    },{
      id: 9,
      label: 'October'
    },{
      id: 10,
      label: 'November'
    },{
      id: 11,
      label: 'December'
    }];


    var month = new Date(vm.trip_client.tripStart).getMonth();
    year = new Date(vm.trip_client.tripStart).getFullYear();
    console.log(vm.trip_client.tripStart);
    console.log(new Date(vm.trip_client.tripStart).getMonth());
    console.log(year);

    $scope.selected_month = $scope.months[month];
    $scope.selected_year = { year: year };      



    // update client trip after change
    var updateDates = function() {
      vm.trip_client.tripStart = new Date($scope.selected_year.year, $scope.selected_month.id, 1, 0, 0, 0, 0).toJSON();
    };

    $scope.$watch('selected_year', function() {
      updateDates();
    }); 
    $scope.$watch('selected_month', function() {
      updateDates();
    });

    /**********/
    /* PLACES */
    /**********/

    var saveNewPlace = function(place, key) {
      place.$save(successCallback, errorCallback);

      function successCallback(res) {
        // Update client_trip only after new place was added
        $scope.convertSelectToStorableArray(key);
        //console.log('place created');
      }

      function errorCallback(res) {
        //console.log('error');
      }
    };

    var updatePlace = function(place) {
      place.$update(successCallback, errorCallback);

      function successCallback(res) {
        //console.log('place updated');
      }

      function errorCallback(res) {
        //console.log('error');
      }
    };

    var deleteNewPlace = function(key) {
      if(typeof vm.newPlacesArray[key] !== 'undefined') {
        if(typeof vm.newPlacesArray[key].object !== 'undefined') {
          vm.newPlacesArray[key].object.$remove();
        }
      }
    };

    $scope.placeChanged = function (key, key2) {
      //console.log(key, key2);

      deleteNewPlace(key);
      vm.newPlacesArray[key] = { name: '', show: false };

      vm.selectedArray[key].splice(key2 + 1, Number.MAX_VALUE);
      vm.placesBlocks[key].splice(key2 + 1, Number.MAX_VALUE);
      var currentValue = vm.selectedArray[key][vm.selectedArray[key].length - 1];
      if(currentValue === idForOtherOption) {
        vm.newPlacesArray[key].show = true;
        // Create new place object
        vm.newPlacesArray[key].object = newPlace(PlacesService);
        console.log(vm.newPlacesArray[key].object);
        // Set new object initial values
        var parents = vm.selectedArray[key].slice(0, vm.selectedArray[key].length -1);
        parents = [idOfWorld].concat(parents);
        vm.newPlacesArray[key].object.parents = parents;
        vm.newPlacesArray[key].object.parent = parents[parents.length - 1];
        vm.newPlacesArray[key].object.name = '';
        // Save new object
        saveNewPlace(vm.newPlacesArray[key].object, key);
      } else if(currentValue === idForChooseOption) {
        $scope.convertSelectToStorableArray(key);
      } else {
        $scope.getSelectData(key, key2 + 1, currentValue);
        $scope.convertSelectToStorableArray(key);
      }
      //console.log(vm.newPlacesArray);      
    };

    $scope.addPlaceBlock = function () {
      vm.placesBlocks.push([]);
      vm.selectedArray.push([]);
      //console.log(vm.selectedArray);
      var i = vm.placesBlocks.length - 1;
      $scope.getSelectData(i, 0, idOfWorld);
    };

    $scope.convertSelectToStorableArray = function (key) {
      var places = [idOfWorld].concat(vm.selectedArray[key]);
      if (places[places.length - 1] === idForChooseOption) {
        places.splice(places.length - 1, Number.MAX_VALUE);
      } else if (places[places.length - 1] === idForOtherOption) {
        places[places.length - 1] = vm.newPlacesArray[key].object._id;
      }
      console.log(vm2.parent.trip_client.places);
      vm2.parent.trip_client.places[key] = { place: places };
      //vm2.place.parents = parents;
      //vm2.place.parent = parents[parents.length - 1];
    };

    /**
     * Convert places data of trip_client to data structure
     * of select
     */
    $scope.convertStorableArrayToSelect = function () {
      var numPlacesBlocks = vm2.parent.trip_client.places.length;
      var place;
      // External for is places blocks
      for(var key = 0; key < numPlacesBlocks; key++) {
        vm.placesBlocks[key] = [];
        vm.selectedArray[key] = [];
        // Internal for is each place in place block
        for(var i = 0; i < vm2.parent.trip_client.places[key].place.length; i++) {
          place = vm2.parent.trip_client.places[key].place[i];
          $scope.getSelectData(key, i, place);
          vm.selectedArray[key][i - 1] = place;
        }
      }
    };

    $scope.getSelectData = function (key, key2, placeId) {
      //console.log("getSelectData(" + key + ", " + key2 + ", " + placeId + ")");
      $http.get('api/places/parent/' + placeId).success(function (response) {
        var childs = response;
        var places;
        var name;
        var other = '';
        
        if(childs.length === 0) {
          getPlace(placeId, PlacesService).then(function(data) {
            if(data.approval === false) {
              vm.newPlacesArray[key] = {
                object: data,
                name: data.name,
                show: true
              };
              vm.selectedArray[key][key2 - 1] = idForOtherOption;
              vm.placesBlocks[key].splice(key2, 1);
            }
          });
        }

        if(key2 === 0) {
          name = '- Continent -';
        } else if (key2 === 1) {
          name = '- Country -';
        } else {
          name = 'All regions';
          other = 'Other region';
        }
        places = [{ _id: idForChooseOption, name: name }];
        places = places.concat(childs);
        if(other !== '') {
          places = places.concat([{ _id: idForOtherOption, name: other }]);
        }
        vm.placesBlocks[key][key2] = { places: places };
        vm.selectedArray[key][vm.selectedArray[key].length] = idForChooseOption;
        //console.log("getSelectData(" + key + ", " + key2 + ", " + placeId + ") - DONE");
      });
    };

    $scope.deletePlaceBlock = function (key) {
      if(confirm('Delete place?')) {

        deleteNewPlace(key);

        vm.placesBlocks.splice(key, 1);
        vm.selectedArray.splice(key, 1);
        vm.newPlacesArray.splice(key, 1);
        vm2.parent.trip_client.places.splice(key, 1);
      }
    };

    // Auto saving of new places
    $scope.$watch('newPlacesArray', function(value, oldValue) {
      for(var i = 0; i < value.length; i++) {
        if(typeof value[i] !== 'undefined') {
          if(typeof value[i].object !== 'undefined') {
            if(value[i].object.name !== value[i].name) {
              value[i].object.name = value[i].name;
              updatePlace(value[i].object);
            }
          }
        }
      }
    }, true);

    if(vm.placesFirstLoad) {
      $scope.convertStorableArrayToSelect();
      vm.placesFirstLoad = false;
    }
    

  }

  newPlace.$inject = ['PlacesService'];

  function newPlace(PlacesService) {
    return new PlacesService();
  }

  getPlace.$inject = ['place_id', 'PlacesService'];

  function getPlace(place_id, PlacesService) {
    return PlacesService.get({
      placeId: place_id
    }).$promise;
  }

})();
