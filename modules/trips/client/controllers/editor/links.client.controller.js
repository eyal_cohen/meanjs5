(function () {
  'use strict';

  angular
    .module('trips')
    .controller('EditorLinksController', EditorLinksController);

  EditorLinksController.$inject = ['$scope', '$state', '$timeout', '$sce', 'Authentication'];

  function EditorLinksController($scope, $state, $timeout, $sce, Authentication) {

    var vm = $scope.$parent;
    var vm2 = this;

    vm2.parent = vm;

    vm2.addLink = function() {
      vm.trip_client.links.push(
        { url: '', description: '' }
      );
    };

    vm2.deleteLink = function (index) {
      if(confirm('Delete link?')) {
        vm.trip_client.links.splice(index ,1);
      }
    };

  }
})();
