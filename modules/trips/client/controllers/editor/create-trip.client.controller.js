(function () {
  'use strict';

  angular
    .module('trips')
    .controller('CreateTripController', CreateTripController);

  CreateTripController.$inject = ['$scope', '$state', 'tripResolve', 'Authentication'];

  function CreateTripController($scope, $state, trip, Authentication) {

    save(true);

    // Save Trip
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.tripForm');
        return false;
      }
      console.log(trip);
      // TODO: move create/update logic to service
      if (trip._id) {
        trip.$update(successCallback, errorCallback);
      } else {
        trip.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('editor.titles', { tripId: trip._id });
      }

      function errorCallback(res) {
        console.log(res.data.message);
      }
    }

  }
})();
