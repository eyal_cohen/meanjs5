(function () {
  'use strict';

  angular
    .module('trips')
    .controller('EditorMapController', EditorMapController);

  EditorMapController.$inject = ['$scope', '$state', '$compile', '$timeout', '$sce', 'leafletData', 'Authentication'];

  function EditorMapController($scope, $state, $compile, $timeout, $sce, leafletData, Authentication) {
    var vm2 = this;
    vm2.parent = $scope.$parent;
    vm2.leaflet = vm2.parent.leaflet;

    // Map layers alternative storage (not the leaflet one)
    $scope.mapLayers = [];

    ///$scope.fix = function(){ console.log("HERE"); };

    // ========
    // = Path =
    // ========
    $scope.pathColors = [
      {
        name: 'Red',
        color: '#ff0000'
      },
      {
        name: 'Blue',
        color: '#0000ff'
      },
      {
        name: 'Black',
        color: '#000000'
      },
      {
        name: 'Orange',
        color: '#ff9900'
      },
      {
        name: 'Purpel',
        color: '#cc00cc'
      },
      {
        name: 'Brown',
        color: '#cc6633'
      },
      {
        name: 'Green',
        color: '#006633'
      }
    ];


    // =========
    // = Icons =
    // =========
    var MapIcon = window.L.Icon.extend({
      options: {
        shadowUrl: '',
        iconSize:     [29, 36],
        shadowSize:   [0, 0],
        iconAnchor:   [14, 38],
        shadowAnchor: [0, 0],
        popupAnchor:  [0, -35]
      }
    });

    // marker colors
    $scope.iconColors = [
      {
        name: 'Black',
        color: 'black'
      },
      {
        name: 'Blue',
        color: 'blue'
      },
      {
        name: 'Green',
        color: 'green'
      },
      {
        name: 'Purpel',
        color: 'purpel'
      },
      {
        name: 'Red',
        color: 'red'
      }
    ];

    // marker icons
    $scope.iconTypes = [
      {
        name: 'Marker',
        file: 'marker'
      },
      {
        name: 'Bridge',
        file: 'bridge'
      },
      {
        name: 'Dive Site',
        file: 'dive-site'
      },
      {
        name: 'Fish',
        file: 'fish'
      },
      {
        name: 'Flag',
        file: 'flag'
      },
      {
        name: 'Food',
        file: 'food'
      },
      {
        name: 'Museum',
        file: 'museum'
      },
      {
        name: 'Hotel',
        file: 'hotel'
      },
      {
        name: 'Turist information',
        file: 'info'
      },
      {
        name: 'Pass / Saddle',
        file: 'pass'
      },
      {
        name: 'Peak / Mountain',
        file: 'peak'
      },
      {
        name: 'Attraction',
        file: 'star'
      },
      {
        name: 'Tent',
        file: 'tent'
      },
      {
        name: 'Water',
        file: 'water'
      },
      {
        name: 'Wreck',
        file: 'wreck'
      },
      {
        name: 'Danger',
        file: 'danger'
      }
    ];

    var iconsPath = 'modules/trips/client/img/map/markers/';

    $scope.icons = [];
    var iconType;
    var colors;
    for(var i in $scope.iconTypes) {
      iconType = $scope.iconTypes[i];
      colors = [];
      for(var j in $scope.iconColors) {
        //console.log($scope.iconColors[j].color);
        colors[$scope.iconColors[j].color] = new MapIcon({ iconUrl: iconsPath + $scope.iconColors[j].color + '/' + iconType.file + '.png' });
      }
      $scope.icons[iconType.file] = {
        name: iconType.name,
        color: colors
      };
    }

    // =================
    // = Map functions =
    // =================

    $scope.pathColorChanged = function (color, layer_id) {
      //console.log(color);
      $scope.mapLayers[layer_id].color = color;
      var layer = $scope.mapLayers[layer_id].layer;
      var style = {
        opacity: 0.8,
        color: color
      };
      layer.setStyle(style).redraw();
    };

    $scope.markerColorChanged = function (color, layer_id) {
      var layer = $scope.mapLayers[layer_id].layer;
      $scope.mapLayers[layer_id].color = color;
      layer.setIcon($scope.icons[$scope.mapLayers[layer_id].icon.file].color[color.color]);
    };

    $scope.markerIconChanged = function (icon, layer_id) {
      var layer = $scope.mapLayers[layer_id].layer;
      $scope.mapLayers[layer_id].icon = icon;
      layer.setIcon($scope.icons[icon.file].color[$scope.mapLayers[layer_id].color.color]);
    };

    // Save map
    var saveMap = function() {
      var layer_id, layer, geojson, latlngs, latlngsArray;
      $scope.$parent.trip_client.map = [];
      for (layer_id in $scope.mapLayers) {
        layer = $scope.mapLayers[layer_id].layer;
        if (typeof layer._latlng !== 'undefined') {
          // This is a marker
          geojson = {
            'type': 'Feature',
            'properties': {
              'description': $scope.mapLayers[layer._leaflet_id].description,
              'color': $scope.mapLayers[layer._leaflet_id].color.name,
              'icon': $scope.mapLayers[layer._leaflet_id].icon.file
            },
            'geometry': {
              'type': 'Point',
              'coordinates': [layer._latlng.lng, layer._latlng.lat]
            }
          };
          $scope.$parent.trip_client.center = {
            lat: layer._latlng.lat,
            lng: layer._latlng.lng
          };
        } else {
          // This is a path
          latlngs = layer.getLatLngs();
          latlngsArray = [];
          for(var i = 0; i < latlngs.length; i++) {
            latlngsArray.push([latlngs[i].lng, latlngs[i].lat]);
          }
          geojson = {
            'type': 'Feature',
            'properties': {
              'description': $scope.mapLayers[layer._leaflet_id].description,
              'color': $scope.mapLayers[layer._leaflet_id].color
            },
            'geometry': {
              'type': 'LineString',
              'coordinates': latlngsArray
            }
          };
        }
        $scope.$parent.trip_client.map.push(geojson);
        console.log(JSON.stringify(geojson));
        console.log($scope.$parent.trip_client);
      }
    };

    $scope.popupSave = function (layer_id) {
      saveMap();
      $scope.mapLayers[layer_id].layer.closePopup();
    };

    var addLayerToMapLayers = function (layer, type, popup) {
      //console.log(layer);
      if (type === 'marker') {
        $scope.mapLayers[layer._leaflet_id] = 
          {
            'layer': layer,
            'description' : '',
            'color': $scope.iconColors[0], // black
            'icon': $scope.iconTypes[0]
          };
        //console.log($scope.mapLayers[layer._leaflet_id]);
        layer.bindPopup('' +
          '<div style="min-width: 300px;"><form>' +
            '<div class="form-group">' +
              '<label>Description</label><br>' +
              '<textarea style="width:100%" ng-model="mapLayers[' + layer._leaflet_id + '].description"></textarea>' +
            '</div>' +
            '<div class="form-group">' +
              '<label>Type</label><br>' +
              '<select style="width:100%" ng-model="mapLayers[' + layer._leaflet_id + '].icon" ng-options="icon.name for icon in iconTypes" ng-change="markerIconChanged(mapLayers[' + layer._leaflet_id + '].icon, ' + layer._leaflet_id + ')"></select>' +
            '</div>' +
            '<div class="form-group">' +
              '<label>Color</label><br>' +
              '<select style="width:100%" ng-model="mapLayers[' + layer._leaflet_id + '].color" ng-options="color.name for color in iconColors" ng-change="markerColorChanged(mapLayers[' + layer._leaflet_id + '].color, ' + layer._leaflet_id + ')"></select>' +
            '</div>' +
            '<hr />' +
            '<div class="form-group">' +
              '<button ng-click="popupSave(' + layer._leaflet_id + ')">Save</button>' +
            '</div>' +
          '</form></div>'
        );
      } else if (type === 'polyline') {
        $scope.mapLayers[layer._leaflet_id] = 
          {
            'layer': layer,
            'description': '',
            'color': $scope.pathColors[0].color,
            'weight': 2
          }
        ;
        layer.bindPopup('' +
          '<div style="min-width: 300px;"><form>' +
            '<div class="form-group">' +
              '<label>Description</label><br>' +
              '<textarea style="width:100%" ng-model="mapLayers[' + layer._leaflet_id + '].description"></textarea>' +
            '</div>' +
            '<div class="form-group">' +
              '<label>Color</label><br>' +
              '<select style="width:100%"  ng-model="mapLayers[' + layer._leaflet_id + '].color" ng-options="color.color as color.name for color in pathColors" ng-change="pathColorChanged(mapLayers[' + layer._leaflet_id + '].color, ' + layer._leaflet_id + ')"></select>' +
            '</div>' +
            '<hr />' +
            '<div class="form-group">' +
              '<button ng-click="popupSave(' + layer._leaflet_id + ')">Save</button>' +
            '</div>' +
          '</form></div>'
        );
      }
      if(popup) {
        layer.openPopup();
      }
    };


    // ==============================
    // = Load saved features to map =
    // ==============================

    $scope.featuresLoaded = false;
    $scope.loadFeaturesToMap = function(map, drawnItems) {
      $scope.featuresLoaded = true;
      
      var isMapEmpty = true;

      var server_geojson = { 'type': 'FeatureCollection', 'features': [] };
      server_geojson.features = $scope.$parent.trip_client.map;
      drawnItems.addData(server_geojson);

      drawnItems.eachLayer(function (layer) {
        isMapEmpty = false;
        var type = 'marker';
        if (typeof layer._latlng === 'undefined') {
          type = 'polyline';
        }
        addLayerToMapLayers(layer, type, false);

        $scope.mapLayers[layer._leaflet_id].description = layer.feature.properties.description;

        if(type === 'polyline') {
          $scope.pathColorChanged(layer.feature.properties.color, layer._leaflet_id);
        } else { // type === "marker"
          for(var colorKey = 0; colorKey < $scope.iconColors.length; colorKey++) {
            //console.log(layer.feature.properties.color);
            if($scope.iconColors[colorKey].name === layer.feature.properties.color) {
              break;
            }
          }
          for(var iconKey = 0; iconKey < $scope.iconTypes.length; iconKey++) {
            //console.log(layer.feature.properties.color);
            if($scope.iconTypes[iconKey].file === layer.feature.properties.icon) {
              break;
            }
          }
          $scope.markerColorChanged($scope.iconColors[colorKey], layer._leaflet_id);
          $scope.markerIconChanged($scope.iconTypes[iconKey], layer._leaflet_id);
        }
      });
      if(!isMapEmpty) {
        window.setTimeout(function () {
          map.fitBounds(drawnItems);
        }, 100);
      }
    };


    // Initial map options
    angular.extend($scope, {
      defaults: {
        tileLayer: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
        maxZoom: 14,
        scrollWheelZoom: false
      },
      center: {
        lat: 50,
        lng: 0,
        zoom: 2
      },
      controls: {
      },
      layers: {
        
      }
    });

    // get map object
    leafletData.getMap().then(function(map) {

      $scope.points = [];

      var drawnItems = window.L.geoJson().addTo(map);

      if(!$scope.featuresLoaded) {
        $scope.loadFeaturesToMap(map, drawnItems);
      }

      // ==========================
      // = Angular fix for popups =
      // ==========================

      $scope.$on('leafletDirectiveMap.popupopen', function(event, leafletEvent){
        // Create the popup view when is opened
        var feature = leafletEvent.leafletEvent.popup.options.feature;

        var newScope = $scope.$new();
        newScope.stream = feature;

        $compile(leafletEvent.leafletEvent.popup._contentNode)(newScope);
      });
      
      // ========
      // = Draw =
      // ========

      // Initialise the draw control and pass it the FeatureGroup of editable layers
      var drawControl = new window.L.Control.Draw({
        position: 'topleft',
        draw: {
          polyline: {
            shapeOptions: {
              color: $scope.pathColors[0].color,
              weight: 5,
              opacity: 0.8
            }
          },
          marker: {
            // The default icon (change "marker" and "black" if needed)
            icon: $scope.icons.marker.color.black
          },
          polygon: false,
          circle: false,
          rectangle: false
        },
        edit: {
          featureGroup: drawnItems
        }
      });

      map.addControl(drawControl);

      map.on('draw:created', function (e) {
        var type = e.layerType,
          layer = e.layer;

        drawnItems.addLayer(layer);

        addLayerToMapLayers(layer, type, true);

        saveMap();
      });
      map.on('draw:edited', function(e) {
        e.layers.eachLayer(function(layer) {
          //console.log("edited: " + layer._leaflet_id);
        });
        saveMap();
      });
      map.on('draw:deleted', function(e) {
        e.layers.eachLayer(function(layer) {
          delete $scope.mapLayers[layer._leaflet_id];
          //console.log("deleted: " + layer._leaflet_id);
        });
        saveMap();
      });

      // ===============
      // = File loader =
      // ===============
      // copy of file leaflet.filelayer.js
      // (so it is possible to add to drawnItems layer)

      var FileLoader = window.L.Class.extend({
        includes: window.L.Mixin.Events,
        options: {
          fileSizeLimit: 1024
        },

        initialize: function (map, options) {
          this._map = map;
          window.L.Util.setOptions(this, options);

          this._parsers = {
            'geojson': this._loadGeoJSON,
            'gpx': this._convertToGeoJSON,
            'kml': this._convertToGeoJSON
          };
        },

        load: function (file /* File */) {
          // Check file size
          var fileSize = (file.size / 1024).toFixed(4);
          if (fileSize > this.options.fileSizeLimit) {
            this.fire('data:error', {
              error: new Error('File size exceeds limit (' + fileSize + ' > ' + this.options.fileSizeLimit + 'kb)')
            });
            return;
          }

          // Check file extension
          var ext = file.name.split('.').pop(),
            parser = this._parsers[ext];
          if (!parser) {
            this.fire('data:error', {
              error: new Error('Unsupported file type ' + file.type + '(' + ext + ')')
            });
            return;
          }
          // Read selected file using HTML5 File API
          var reader = new FileReader();
          reader.onload = window.L.Util.bind(function (e) {
            try {
              this.fire('data:loading', { filename: file.name, format: ext });
              var layer = parser.call(this, e.target.result, ext);
              this.fire('data:loaded', { layer: layer, filename: file.name, format: ext });
            }
            catch (err) {
              this.fire('data:error', { error: err });
            }

          }, this);
          reader.readAsText(file);
          return reader;
        },

        _loadGeoJSON: function (content) {
          if (typeof content === 'string') {
            content = JSON.parse(content);
          }

          drawnItems.addData(content);
          //console.log(content);
          drawnItems.eachLayer(function (layer) {
            //console.log(layer);
            if(typeof $scope.mapLayers[layer._leaflet_id] === 'undefined') {
              
              // This is a new layer
              var type = 'marker';
              if (typeof layer._latlng === 'undefined') {
                type = 'polyline';
              }
              addLayerToMapLayers(layer, type, false);
              
              if(type === 'polyline') {
                $scope.pathColorChanged($scope.pathColors[0].color, layer._leaflet_id);
              } else { // type === "marker"
                $scope.markerIconChanged($scope.iconTypes[0], layer._leaflet_id);
              }
              
            }
          });
          saveMap();
          return drawnItems;
        },

        _convertToGeoJSON: function (content, format) {
          // Format is either 'gpx' or 'kml'
          if (typeof content === 'string') {
            content = (new window.DOMParser()).parseFromString(content, 'text/xml');
          }
          var geojson = window.toGeoJSON[format](content);
          return this._loadGeoJSON(geojson);
        }
      });


      window.L.Control.FileLayerLoad = window.L.Control.extend({
        statics: {
          TITLE: 'Load local file (GPX, KML, GeoJSON)',
          LABEL: '&#8965;'
        },
        options: {
          position: 'topleft',
          fitBounds: true,
          layerOptions: {},
          addToMap: true,
          fileSizeLimit: 512
        },

        initialize: function (options) {
          window.L.Util.setOptions(this, options);
          this.loader = null;
        },

        onAdd: function (map) {
          this.loader = new FileLoader(map, this.options);

          this.loader.on('data:loaded', function (e) {
            // Fit bounds after loading
            if (this.options.fitBounds) {
              window.setTimeout(function () {
                map.fitBounds(e.layer.getBounds());
              }, 500);
            }
          }, this);

          // Initialize Drag-and-drop
          this._initDragAndDrop(map);

          // Initialize map control
          return this._initContainer();
        },

        _initDragAndDrop: function (map) {
          var fileLoader = this.loader,
            dropbox = map._container;

          var callbacks = {
            dragenter: function () {
              map.scrollWheelZoom.disable();
            },
            dragleave: function () {
              map.scrollWheelZoom.enable();
            },
            dragover: function (e) {
              e.stopPropagation();
              e.preventDefault();
            },
            drop: function (e) {
              e.stopPropagation();
              e.preventDefault();

              var files = Array.prototype.slice.apply(e.dataTransfer.files),
                i = files.length;
              function callback() {
                fileLoader.load(files.shift());
                if (files.length > 0) {
                  setTimeout(callback, 25);
                }
              }

              setTimeout(callback, 25);

              map.scrollWheelZoom.enable();
            }
          };
          for (var name in callbacks)
            dropbox.addEventListener(name, callbacks[name], false);
        },

        _initContainer: function () {
          // Create a button, and bind click on hidden file input
          var zoomName = 'leaflet-control-filelayer leaflet-control-zoom',
            barName = 'leaflet-bar',
            partName = barName + '-part',
            container = window.L.DomUtil.create('div', zoomName + ' ' + barName);
          var link = window.L.DomUtil.create('a', zoomName + '-in ' + partName, container);
          link.innerHTML = window.L.Control.FileLayerLoad.LABEL;
          link.href = '#';
          link.title = window.L.Control.FileLayerLoad.TITLE;

          // Create an invisible file input
          var fileInput = window.L.DomUtil.create('input', 'hidden', container);
          fileInput.type = 'file';
          if (!this.options.formats) {
            fileInput.accept = '.gpx,.kml,.geojson';
          } else {
            fileInput.accept = this.options.formats.join(',');
          }
          fileInput.style.display = 'none';
          // Load on file change
          var fileLoader = this.loader;
          fileInput.addEventListener('change', function (e) {
            fileLoader.load(this.files[0]);
            // reset so that the user can upload the same file again if they want to
            this.value = '';
          }, false);

          window.L.DomEvent.disableClickPropagation(link);
          window.L.DomEvent.on(link, 'click', function (e) {
            fileInput.click();
            e.preventDefault();
          });
          return container;
        }
      });

      window.L.Control.fileLayerLoad = function (options) {
        return new window.L.Control.FileLayerLoad(options);
      };

      window.L.Control.fileLayerLoad({
        // File size limit in kb (default: 1024) ?
        fileSizeLimit: 512,
        // Restrict accepted file formats (default: .geojson, .kml, and .gpx) ?
        formats: [
          '.geojson',
          '.kml'
        ]
      }).addTo(map);

      var control = window.L.Control.fileLayerLoad();
      
    });

    ///
    console.log($scope);
    //
    //$scope.fix();
  }

})();
