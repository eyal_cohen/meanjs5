(function () {
  'use strict';

  angular
    .module('trips')
    .controller('EditorFilesController', EditorFilesController);

  EditorFilesController.$inject = ['$scope', '$state', '$timeout', '$sce', '$http', 'Authentication', 'Upload'];

  function EditorFilesController($scope, $state, $timeout, $sce, $http, Authentication, Upload) {

    var vm = $scope.$parent;
    var vm2 = this;

    vm2.parent = vm;

    $scope.uploadFiles = function (files) {
      angular.forEach(files, function(c_file, key) {
        var filename = c_file.name;
        var type = c_file.type;
        var query = {
          filename: filename,
          type: type
        };
        //console.log(c_file);
        type = filename.split('.').pop().toLowerCase();
        vm.files_array.push({
          filename: filename,
          server_filename: '',
          filetype: type,
          description: '',
          progress: 0,
          typeimg: 'modules/trips/client/img/editor/filetypes/'+ type +'.png'
        });
        var fileCount = vm.files_array.length - 1;
        //console.log('uploading...');
        $http.post('/aws-signing', query)
          .success(function(result) {
            Upload.upload({
              url: result.url, //s3Url
              transformRequest: function(data, headersGetter) {
                var headers = headersGetter();
                delete headers.Authorization;
                return data;
              },
              fields: result.fields, // credentials
              method: 'POST',
              acl: 'public-read',
              file: c_file
            }).progress(function(evt) {
              var progress = parseInt(100.0 * evt.loaded / evt.total);
              vm.files_array[fileCount].progress = progress;
            }).success(function(data, status, headers, config) {
              // file is uploaded successfully

              console.log('success');
              
              // convert xml to json
              var x2js = new window.X2JS();
              var xmlText = data;
              var jsonObj = x2js.xml_str2json(xmlText);
              console.log(jsonObj);

              var server_filename = jsonObj.PostResponse.Key;
              vm.files_array[fileCount].server_filename = server_filename;
              console.log(vm.files_array);

              vm2.updateFilesArray();

            }).error(function(headers) {
              console.log(headers);
            });
          })
          .error(function(data, status, headers, config) {
            // Amazon auth error
            console.log('error');
          });
      });
    };

    vm2.updateFilesArray = function() {
      vm.trip_client.files = [];
      angular.forEach(vm.files_array, function(file, key) {
        vm.trip_client.files.push({
          filename: file.filename,
          server_filename: file.server_filename,
          filetype: file.filetype,
          description: file.description
        });
      });
    };

    vm2.deleteFile = function (index) {
      if(confirm('Delete file?')) {
        vm.files_array.splice(index ,1);
        vm.trip_client.files.splice(index ,1);
      }
    };

    $scope.$watch('vm2.parent.files_array', function() {
      $timeout(vm2.updateFilesArray, 500);
    }, true);
  }
})();
