(function () {
  'use strict';

  angular
    .module('trips')
    .controller('EditorTipsController', EditorTipsController);

  EditorTipsController.$inject = ['$scope', '$state', '$timeout', '$sce', 'Authentication'];

  function EditorTipsController($scope, $state, $timeout, $sce, Authentication) {

    var vm = $scope.$parent;
    var vm2 = this;

    vm2.parent = vm;

    vm2.addTip = function() {
      vm.trip_client.tips.push(
        { body: '' }
      );
    };

    vm2.deleteTip = function (index) {
      if(confirm('Delete tip?')) {
        vm.trip_client.tips.splice(index ,1);
      }
    };

  }
})();
