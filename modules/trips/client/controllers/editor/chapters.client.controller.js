(function () {
  'use strict';

  angular
    .module('trips')
    .controller('EditorChaptersController', EditorChaptersController);

  EditorChaptersController.$inject = ['$scope', '$state', '$timeout', '$sce', '$http', 'Authentication', 'Upload'];

  function EditorChaptersController($scope, $state, $timeout, $sce, $http, Authentication, Upload) {

    var vm = $scope.$parent;
    var vm2 = this;

    vm2.parent = vm;

    // Do not allow url change to chapter with negative value or too large one
    if($state.params.chapterId > vm.trip_client.chapters.length || $state.params.chapterId < 1) {
      $state.go('editor.chapters', { chapterId: 1 });
    }
    if($state.params.chapterId === '') {
      $state.go('editor.chapters', { chapterId: 1 });
    }
    vm2.currentChapter = $state.params.chapterId - 1;
    vm.lastEditedChapter = $state.params.chapterId;

    $scope.chapter_images = $scope.$parent.chapter_images;

    vm2.addChapter = function() {
      vm.trip_client.chapters.push(
        {
          'title': '',
          'body': '',
          'images': [],
          'videos': []
        }
      );
      $state.go('editor.chapters', { chapterId: vm.trip_client.chapters.length });
    };

    vm2.deleteChapter = function (index) {
      if(vm.trip_client.chapters.length !== 1) { // Do not delete last chapter
        if(confirm('Delete chapter?')) {
          vm.trip_client.chapters.splice(index ,1);
          $scope.chapter_images.splice(index, 1);

          var id = 1;
          if(index === vm.trip_client.chapters.length) {
            id = index;
          } else {
            id = index + 1;
          }
          $state.go('editor.chapters', { chapterId: id });
        }
      }
    };

    vm2.addVideo = function() {
      vm.trip_client.chapters[vm2.currentChapter].videos.push(
        { url: '', description: '' }
      );
    };

    vm2.deleteVideo = function (index) {
      if(confirm('Delete video?')) {
        vm.trip_client.chapters[vm2.currentChapter].videos.splice(index ,1);
      }
    };


    vm2.deleteImage = function (index) {
      if(confirm('Delete image?')) {
        $scope.chapter_images[vm2.currentChapter].splice(index ,1);
      }
    };

    // Add first chapter if no chapter exists
    if(vm.trip_client.chapters.length === 0) {
      vm2.addChapter();
    }


    // >>> Image uploader >>>
    $scope.files = [];

    // create blitline job
    var blitline_job = function(filename) {
      var job = {
        application_id: '4c6wAlWKtaSfxBt-leO7gQA',
        src: { 
          'name': 's3', 
          'bucket': 'triptip-files',
          'key': filename
        },
        v: '1.22',
        functions: [
          {
            'name': 'resize_to_fit',
            'params': {
              'width': '2048',
              'height': '1365'
            },
            'save': {
              'image_identifier': 'triptip',
              's3_destination' : {
                'bucket' : 'triptip-files',
                'key' : '2048_1365/' + filename
              }
            }  
          },
          {
            'name': 'resize_to_fit',
            'params': {
              'width': '1200',
              'height': '800'
            },
            'save': {
              'image_identifier': 'triptip',
              's3_destination' : {
                'bucket' : 'triptip-files',
                'key' : '1200_800/' + filename
              }
            }  
          },
          {
            'name': 'pad_resize_to_fit',
            'params': {
              'width': '450',
              'height': '300',
              'color': '#ffffffff'
            },
            'save': {
              'image_identifier': 'triptip',
              's3_destination' : {
                'bucket' : 'triptip-files',
                'key' : '450_300-pad/' + filename
              }
            }
          },
          {
            'name': 'resize_to_fill',
            'params': {
              'width': '400',
              'height': '300'
            },
            'save': {
              'image_identifier': 'triptip',
              's3_destination' : {
                'bucket' : 'triptip-files',
                'key' : '400_300/' + filename
              }
            }
          },
          {
            'name': 'resize_to_fill',
            'params': {
              'width': '200',
              'height': '150'
            },
            'save': {
              'image_identifier': 'MY_CLIENT_ID2',
              's3_destination' : {
                'bucket' : 'triptip-files',
                'key' : '200_150/' + filename
              }
            }
          }
        ]
      };
      return job;
    };

    // The "callback" argument is called with either true or false
    // depending on whether the image at "url" exists or not.
    function imageExists(url, callback) {
      var img = new Image();
      img.onload = function() { callback(true); };
      img.onerror = function() { callback(false); };
      img.src = url;
    }

    function setImageServerName(key1, key2, server_filename) {
      $timeout(function() {
        // the url assigned here and not in view because of a bug in ng-src
        var imageUrl = 'http://s3-us-west-2.amazonaws.com/triptip-files/450_300-pad/' + server_filename;
        imageExists(imageUrl, function(exists) {
          if(!exists) {
            setImageServerName(key1, key2, server_filename);
          } else {
            vm.chapter_images[key1][key2].full_url = imageUrl;
            vm.chapter_images[key1][key2].file = server_filename; // this is the server file name
            $scope.$apply();
          }
        });
      }, 1500);
    }

    $scope.convertStorableArrayToEditable = function () {
      vm.chapter_images = [];
      
      angular.forEach(vm.trip_client.chapters, function(chapter, key) {
        vm.chapter_images[key] = [];
        
        angular.forEach(chapter.images, function(image, key2) {
          vm.chapter_images[key].push({
            file: image.file, // this is the server file name
            full_url: 'http://s3-us-west-2.amazonaws.com/triptip-files/450_300-pad/' + image.file,
            description: image.description,
            progress: 100
          });
        });
        $scope.chapter_images = $scope.$parent.chapter_images;
      });
    };

    // recover saved images
    if(!vm.chapter_images_loaded_flag) {
      $scope.convertStorableArrayToEditable();
      vm.chapter_images_loaded_flag = 1;
    }
    

    $scope.uploadFiles = function (files) {
      // Create image array if it's not exists yet
      if(!(vm.chapter_images[$state.params.chapterId - 1] instanceof Array)) {
        vm.chapter_images[$state.params.chapterId - 1] = [];
      }

      // for each file - upload
      angular.forEach(files, function(c_file, key) {
        console.log(c_file);
        if(c_file.size > 20000000) {
          console.log('file too big');
        } else {
          var filename = c_file.name;
          var type = c_file.type;
          var query = {
            filename: filename,
            type: type
          };

          // insert file to array
          vm.chapter_images[$state.params.chapterId - 1].push({
            file: '', // this is the server file name
            full_url: '',
            description: '',
            progress: 0
          });

          // calc image count
          var imageCount = vm.chapter_images[$state.params.chapterId - 1].length - 1;

        // Sign to amazon and on seccess upload the file
          $http.post('/aws-signing', query)
            .success(function(result) {
              var currentChapter2 = $state.params.chapterId - 1;

              Upload.upload({
                url: result.url, //s3Url
                transformRequest: function(data, headersGetter) {
                  var headers = headersGetter();
                  delete headers.Authorization;
                  return data;
                },
                fields: result.fields, // credentials
                method: 'POST',
                acl: 'public-read',
                file: c_file
              }).progress(function(evt) {
                var progress = parseInt(100.0 * evt.loaded / evt.total);
                vm.chapter_images[currentChapter2][imageCount].progress = progress;
              }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                // convert xml to json
                var x2js = new window.X2JS();
                var xmlText = data;
                var jsonObj = x2js.xml_str2json(xmlText);
                var server_filename = jsonObj.PostResponse.Key;

                // make blitline api call to create thumbnails
                var job_data = blitline_job(server_filename);
                $http.post('/blitline', JSON.stringify(job_data)).
                  success(function(data, status, headers, config) {
                    // set file name. timeout to let the file be available on amazon
                    setImageServerName(currentChapter2, imageCount, server_filename);

                    // create image object to save on db
                    var desc = vm.chapter_images[currentChapter2][imageCount].description;

                    // add image to actual trip object (this will make saving)
                    vm.trip_client.chapters[currentChapter2].images.push({
                      file: server_filename,
                      description: desc
                    });

                  }).
                  error(function(data, status, headers, config) {
                    //console.log(data);
                  });
              }).error(function(headers) {
                //console.log(headers);
              });
            })
            .error(function(data, status, headers, config) {
              // Amazon auth error
              //console.log('error');
            });
        }
      });
    };
    
    vm2.updateFilesArray = function() {
      vm.trip_client.chapters[vm2.currentChapter].images = [];
      angular.forEach(vm.chapter_images[vm2.currentChapter], function(file, key) {
        if(file.file !== '') {
          vm.trip_client.chapters[vm2.currentChapter].images.push({
            file: file.file, // this is the server file name
            description: file.description
          });
        }
      });
      
    };
    
    $scope.$watch('chapter_images', function() {
      $timeout(vm2.updateFilesArray, 500);
    }, true);
    
  }
})();