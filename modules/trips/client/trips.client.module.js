(function (app) {
  'use strict';

  app.registerModule('trips');
  app.registerModule('trips.services');
  app.registerModule('trips.routes', ['ui.router', 'trips.services']);
  app.registerModule('trips.admin.routes', ['core.admin.routes']);
  app.registerModule('trips.admin', ['core.admin']);
})(ApplicationConfiguration);
