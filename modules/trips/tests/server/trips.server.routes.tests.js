'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Trip = mongoose.model('Trip'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, trip;

/**
 * Trip routes tests
 */
describe('Trip CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new trip
    user.save(function () {
      trip = {
        title: 'Trip Title',
        content: 'Trip Content'
      };

      done();
    });
  });

  it('should be able to save an trip if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new trip
        agent.post('/api/trips')
          .send(trip)
          .expect(200)
          .end(function (tripSaveErr, tripSaveRes) {
            // Handle trip save error
            if (tripSaveErr) {
              return done(tripSaveErr);
            }

            // Get a list of trips
            agent.get('/api/trips')
              .end(function (tripsGetErr, tripsGetRes) {
                // Handle trip save error
                if (tripsGetErr) {
                  return done(tripsGetErr);
                }

                // Get trips list
                var trips = tripsGetRes.body;

                // Set assertions
                (trips[0].user._id).should.equal(userId);
                (trips[0].title).should.match('Trip Title');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an trip if not logged in', function (done) {
    agent.post('/api/trips')
      .send(trip)
      .expect(403)
      .end(function (tripSaveErr, tripSaveRes) {
        // Call the assertion callback
        done(tripSaveErr);
      });
  });

  it('should not be able to save an trip if no title is provided', function (done) {
    // Invalidate title field
    trip.title = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new trip
        agent.post('/api/trips')
          .send(trip)
          .expect(400)
          .end(function (tripSaveErr, tripSaveRes) {
            // Set message assertion
            (tripSaveRes.body.message).should.match('Title cannot be blank');

            // Handle trip save error
            done(tripSaveErr);
          });
      });
  });

  it('should be able to update an trip if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new trip
        agent.post('/api/trips')
          .send(trip)
          .expect(200)
          .end(function (tripSaveErr, tripSaveRes) {
            // Handle trip save error
            if (tripSaveErr) {
              return done(tripSaveErr);
            }

            // Update trip title
            trip.title = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing trip
            agent.put('/api/trips/' + tripSaveRes.body._id)
              .send(trip)
              .expect(200)
              .end(function (tripUpdateErr, tripUpdateRes) {
                // Handle trip update error
                if (tripUpdateErr) {
                  return done(tripUpdateErr);
                }

                // Set assertions
                (tripUpdateRes.body._id).should.equal(tripSaveRes.body._id);
                (tripUpdateRes.body.title).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of trips if not signed in', function (done) {
    // Create new trip model instance
    var tripObj = new Trip(trip);

    // Save the trip
    tripObj.save(function () {
      // Request trips
      request(app).get('/api/trips')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single trip if not signed in', function (done) {
    // Create new trip model instance
    var tripObj = new Trip(trip);

    // Save the trip
    tripObj.save(function () {
      request(app).get('/api/trips/' + tripObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('title', trip.title);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single trip with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/trips/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Trip is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single trip which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent trip
    request(app).get('/api/trips/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No trip with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an trip if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new trip
        agent.post('/api/trips')
          .send(trip)
          .expect(200)
          .end(function (tripSaveErr, tripSaveRes) {
            // Handle trip save error
            if (tripSaveErr) {
              return done(tripSaveErr);
            }

            // Delete an existing trip
            agent.delete('/api/trips/' + tripSaveRes.body._id)
              .send(trip)
              .expect(200)
              .end(function (tripDeleteErr, tripDeleteRes) {
                // Handle trip error error
                if (tripDeleteErr) {
                  return done(tripDeleteErr);
                }

                // Set assertions
                (tripDeleteRes.body._id).should.equal(tripSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an trip if not signed in', function (done) {
    // Set trip user
    trip.user = user;

    // Create new trip model instance
    var tripObj = new Trip(trip);

    // Save the trip
    tripObj.save(function () {
      // Try deleting trip
      request(app).delete('/api/trips/' + tripObj._id)
        .expect(403)
        .end(function (tripDeleteErr, tripDeleteRes) {
          // Set message assertion
          (tripDeleteRes.body.message).should.match('User is not authorized');

          // Handle trip error error
          done(tripDeleteErr);
        });

    });
  });

  it('should be able to get a single trip that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new trip
          agent.post('/api/trips')
            .send(trip)
            .expect(200)
            .end(function (tripSaveErr, tripSaveRes) {
              // Handle trip save error
              if (tripSaveErr) {
                return done(tripSaveErr);
              }

              // Set assertions on new trip
              (tripSaveRes.body.title).should.equal(trip.title);
              should.exist(tripSaveRes.body.user);
              should.equal(tripSaveRes.body.user._id, orphanId);

              // force the trip to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the trip
                    agent.get('/api/trips/' + tripSaveRes.body._id)
                      .expect(200)
                      .end(function (tripInfoErr, tripInfoRes) {
                        // Handle trip error
                        if (tripInfoErr) {
                          return done(tripInfoErr);
                        }

                        // Set assertions
                        (tripInfoRes.body._id).should.equal(tripSaveRes.body._id);
                        (tripInfoRes.body.title).should.equal(trip.title);
                        should.equal(tripInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Trip.remove().exec(done);
    });
  });
});
