'use strict';

module.exports = {
  client: {
    lib: {
      css: [
        'public/lib/bootstrap/dist/css/bootstrap.css',
        'public/lib/Leaflet.Elevation/dist/leaflet.elevation-0.0.4.css',
        'public/lib/Leaflet.draw/dist/leaflet.draw.css',
        'public/lib/PruneCluster/dist/LeafletStyleSheet.css',
        'public/lib/Leaflet.fullscreen/dist/Leaflet.fullscreen.css',
        'public/lib/leaflet/leaflet.css',
        'public/lib/PhotoSwipe/dist/photoswipe.css',
        'public/lib/PhotoSwipe/dist/default-skin/default-skin.css',
        'public/lib/bootstrap/dist/css/bootstrap-theme.css'
      ],
      js: [
        'public/lib/d3/d3.min.js',
        'public/lib/leaflet/leaflet.js',
        'public/lib/angular/angular.js',
        'public/lib/angular-resource/angular-resource.js',
        'public/lib/angular-animate/angular-animate.js',
        'public/lib/angular-messages/angular-messages.js',
        'public/lib/angular-ui-router/release/angular-ui-router.js',
        'public/lib/angular-ui-utils/ui-utils.js',
        'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
        'public/lib/angular-file-upload/angular-file-upload.js',
        'public/lib/owasp-password-strength-test/owasp-password-strength-test.js',
        'public/lib/angular-leaflet-directive/dist/angular-leaflet-directive.js',
        'public/lib/angular-sortable-view/src/angular-sortable-view.js',
        'public/lib/Leaflet.draw/dist/leaflet.draw.js',
        'public/lib/Leaflet.Elevation/dist/leaflet.elevation-0.0.4.min.js',
        'public/lib/Leaflet.FileLayer/leaflet.filelayer.js',
        'public/lib/Leaflet.FileLayer/gpx.js',
        'public/lib/togeojson/togeojson.js',
        'public/lib/Leaflet.fullscreen/dist/Leaflet.fullscreen.min.js',
        'public/lib/PruneCluster/dist/PruneCluster.js',
        'public/lib/ng-file-upload/dist/ng-file-upload-shim.min.js',
        'public/lib/ng-file-upload/dist/ng-file-upload.min.js',
        'public/lib/x2js/xml2json.min.js',
        'public/lib/PhotoSwipe/dist/photoswipe.min.js',
        'public/lib/PhotoSwipe/dist/photoswipe-ui-default.min.js',
        'public/lib/angular-elastic/elastic.js'
      ],
      tests: ['public/lib/angular-mocks/angular-mocks.js']
    },
    css: [
      'modules/*/client/css/*.css'
    ],
    less: [
      'modules/*/client/less/*.less'
    ],
    sass: [
      'modules/*/client/scss/*.scss'
    ],
    js: [
      'modules/core/client/app/config.js',
      'modules/core/client/app/init.js',
      'modules/*/client/*.js',
      'modules/*/client/**/*.js'
    ],
    img: [
      'modules/**/*/img/**/*.jpg',
      'modules/**/*/img/**/*.png',
      'modules/**/*/img/**/*.gif',
      'modules/**/*/img/**/*.svg'
    ],
    views: ['modules/*/client/views/**/*.html'],
    templates: ['build/templates.js']
  },
  server: {
    gruntConfig: ['gruntfile.js'],
    gulpConfig: ['gulpfile.js'],
    allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
    models: 'modules/*/server/models/**/*.js',
    routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
    sockets: 'modules/*/server/sockets/**/*.js',
    config: ['modules/*/server/config/*.js'],
    policies: 'modules/*/server/policies/*.js',
    views: ['modules/*/server/views/*.html']
  }
};
